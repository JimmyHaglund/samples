﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/// <summary>
/// A script that allows moving objects using Blender's style of hotkeys in Unity.
/// Good against Carpal Tunnel.
/// </summary>
public class BlenderStyleTools : MonoBehaviour
{
    private static  bool            _enabled            = false;
    private static  KeyCode         _heldKey            = KeyCode.None;
    private static  Vector3         _scaleOffset        = Vector3.zero;
    private static  float           _rotationOffset     = 0.0f;
    private static  Vector3         _rotationOffsetAxis = Vector3.zero;
    private static  Vector3         _mouseMoveTotal     = Vector3.zero;
    private static  List<Vector3>   _selectionStartCoordinates = new List<Vector3>();
    private enum    ToolState
    {
        None,
        ScaleAll,
        ScaleX,
        ScaleY,
        ScaleZ,
        ScaleXY,
        ScaleXZ,
        ScaleYZ,

        TranslateAll,
        TranslateX,
        TranslateY,
        TranslateZ,
        TranslateXY,
        TranslateXZ,
        TranslateYZ,

        TranslateXLocal,
        TranslateYLocal,
        TranslateZLocal,
        TranslateXYLocal,
        TranslateXZLocal,
        TranslateYZLocal,

        RotateAll,
        RotateX,
        RotateY,
        RotateZ,
        RotateXY,
        RotateXZ,
        RotateYZ
    };
    private static  ToolState       _currentState;

    static BlenderStyleTools() {}

    public static void SetEnabled()
    {
        if (_enabled) return;
        _enabled = true;
        Debug.Log("Blender style tools enabled.");
        SceneView.onSceneGUIDelegate += ToolInteraction;
        Selection.selectionChanged += ChangeSelection;
    }
    public static void ToolInteraction(SceneView sceneView)
    {
        Event e = Event.current;
        if (e == null) return;

        

        // Mouse movement
        if (e.type == EventType.MouseMove)
        {
            MouseMove(e.delta);
        }

        // Mouse pressed
        if (e.type == EventType.MouseDown)
        {
            _currentState = ToolState.None;
            ApplyTranslation();
        }

        // Key released.
        if (e.type == EventType.KeyUp)
        {
            _heldKey = KeyCode.None;
            return;
        }

        // Key press
        if (e.type    == EventType.KeyDown
        &&  e.keyCode != _heldKey)
        {
            _heldKey = e.keyCode;
            KeyPress(_heldKey);
        }
    }
    public static void ChangeSelection()
    {
        _selectionStartCoordinates.Clear();
        for (int n = 0; n < Selection.transforms.Length; ++n)
        {
            _selectionStartCoordinates.Add(Selection.transforms[n].position);
        }
    }
    private static void MouseMove(Vector2 mouseMoveVector)
    {
        float distanceToCamera = 10.0f;
        distanceToCamera = (Camera.current.transform.position - Tools.handlePosition).magnitude;
        Vector3 mouseMovementWorld = Camera.current.transform.TransformVector(new Vector3(mouseMoveVector.x, -mouseMoveVector.y, 0.0f)) * distanceToCamera / 500.0f;

        switch (_currentState)
        {
            case ToolState.TranslateAll:
                MoveSelection(mouseMovementWorld);
                break;

            case ToolState.TranslateX:
                MoveSelection(new Vector3(mouseMovementWorld.x, 0.0f, 0.0f));
                break;
            case ToolState.TranslateY:
                MoveSelection(new Vector3(0.0f, mouseMovementWorld.y, 0.0f));
                break;
            case ToolState.TranslateZ:
                MoveSelection(new Vector3(0.0f, 0.0f, mouseMovementWorld.z));
                break;

            case ToolState.TranslateXLocal:
                MoveSelection(Vector3.Project(mouseMovementWorld, Selection.transforms[0].right));
                break;
            case ToolState.TranslateYLocal:
                MoveSelection(Vector3.Project(mouseMovementWorld, Selection.transforms[0].up));
                break;
            case ToolState.TranslateZLocal:
                MoveSelection(Vector3.Project(mouseMovementWorld, Selection.transforms[0].forward));
                break;

            case ToolState.TranslateXY:
                MoveSelection(new Vector3(mouseMovementWorld.x, mouseMovementWorld.y, 0.0f));
                break;
            case ToolState.TranslateXZ:
                MoveSelection(new Vector3(mouseMovementWorld.x, 0.0f, mouseMovementWorld.z));
                break;
            case ToolState.TranslateYZ:
                MoveSelection(new Vector3(0.0f, mouseMovementWorld.y, mouseMovementWorld.z));
                break;

            case ToolState.TranslateXYLocal:
                MoveSelection(Vector3.ProjectOnPlane(mouseMovementWorld, Selection.transforms[0].forward));
                break;
            case ToolState.TranslateXZLocal:
                MoveSelection(Vector3.ProjectOnPlane(mouseMovementWorld, Selection.transforms[0].up));
                break;
            case ToolState.TranslateYZLocal:
                MoveSelection(Vector3.ProjectOnPlane(mouseMovementWorld, Selection.transforms[0].right));
                break;


            case ToolState.ScaleAll:
                ScaleSelection(new Vector3(mouseMoveVector.x, mouseMoveVector.x, mouseMoveVector.x) * 0.01f);
                break;
            case ToolState.ScaleX:
                ScaleSelection(Vector3.Project(mouseMovementWorld, Selection.transforms[0].right));
                break;
            case ToolState.ScaleY:
                ScaleSelection(Vector3.Project(mouseMovementWorld, Selection.transforms[0].up));
                break;
            case ToolState.ScaleZ:
                ScaleSelection(Vector3.Project(mouseMovementWorld, Selection.transforms[0].forward));
                break;
            case ToolState.RotateX:
                RotateSelection(mouseMoveVector.x, Vector3.right);
                break;
            case ToolState.RotateY:
                RotateSelection(mouseMoveVector.x, Vector3.up);
                break;
            case ToolState.RotateZ:
                RotateSelection(mouseMoveVector.x, Vector3.forward);
                break;
            default:
                break;
        }
        // Debug.Log("mouse moved : " + moveVector);
    }
    private static void KeyPress(KeyCode key)
    {
        // Debug.Log("Key press registered: " + key);
        switch (key)
        {
            case KeyCode.Mouse4:
                Tools.current = Tool.Move;
                break;
            case KeyCode.G: // Translate tool
                ResetTranslation();
                Tools.current = Tool.Move;
                Tools.pivotRotation = PivotRotation.Global;
                _currentState = ToolState.TranslateAll;
                break;
            case KeyCode.S: // Scale tool
                ResetTranslation();
                Tools.pivotRotation = PivotRotation.Global;
                Tools.current = Tool.Scale;
                _currentState = ToolState.ScaleAll;
                break;
            case KeyCode.R:
                Tools.current = Tool.Rotate;
                _currentState = ToolState.RotateAll;
                break;

            case KeyCode.X: // X axis
                ResetTranslation();
                if (Tools.current == Tool.Move)
                {
                    if (_currentState == ToolState.TranslateX)
                    {
                        Tools.pivotRotation = PivotRotation.Local;
                        _currentState = ToolState.TranslateXLocal;
                    }
                    else if (Event.current.modifiers == EventModifiers.Shift)
                    {
                        if (_currentState == ToolState.TranslateYZ)
                        {
                            Tools.pivotRotation = PivotRotation.Local;
                            _currentState = ToolState.TranslateYZLocal;
                        }
                        else
                        {
                            Tools.pivotRotation = PivotRotation.Global;
                            _currentState = ToolState.TranslateYZ;
                        }
                    }
                    else
                    {
                        Tools.pivotRotation = PivotRotation.Global;
                        _currentState = ToolState.TranslateX;
                    }
                }
                else if (Tools.current == Tool.Scale)
                {
                    _currentState = ToolState.ScaleX;
                }
                else if (Tools.current == Tool.Rotate)
                {
                    _currentState = ToolState.RotateX;
                    _rotationOffsetAxis = Vector3.right;
                }
                break;
            case KeyCode.Y: // Y axis
                ResetTranslation();
                if (Tools.current == Tool.Move)
                {
                    if (_currentState == ToolState.TranslateY)
                    {
                        Tools.pivotRotation = PivotRotation.Local;
                        _currentState = ToolState.TranslateYLocal;
                    }
                    else if (Event.current.modifiers == EventModifiers.Shift)
                    {
                        if (_currentState == ToolState.TranslateXZ)
                        {
                            Tools.pivotRotation = PivotRotation.Local;
                            _currentState = ToolState.TranslateXZLocal;
                        }
                        else
                        {
                            Tools.pivotRotation = PivotRotation.Global;
                            _currentState = ToolState.TranslateXZ;
                        }
                    }
                    else
                    {
                        Tools.pivotRotation = PivotRotation.Global;
                        _currentState = ToolState.TranslateY;
                    }
                }
                else if (Tools.current == Tool.Scale)
                {
                    _currentState = ToolState.ScaleY;
                }
                else if (Tools.current == Tool.Rotate)
                {
                    _currentState = ToolState.RotateY;
                }
                break;
            case KeyCode.Z: // Z axis
                ResetTranslation();
                if (Tools.current == Tool.Move)
                {
                    if (_currentState == ToolState.TranslateZ)
                    {
                        Tools.pivotRotation = PivotRotation.Local;
                        _currentState = ToolState.TranslateZLocal;
                    }
                    else if (Event.current.modifiers == EventModifiers.Shift)
                    {
                        if (_currentState == ToolState.TranslateXY)
                        {
                            Tools.pivotRotation = PivotRotation.Local;
                            _currentState = ToolState.TranslateXYLocal;
                        }
                        else
                        {
                            Tools.pivotRotation = PivotRotation.Global;
                            _currentState = ToolState.TranslateXY;
                        }
                    }
                    else
                    {
                        Tools.pivotRotation = PivotRotation.Global;
                        _currentState = ToolState.TranslateZ;
                    }
                }
                else if (Tools.current == Tool.Scale)
                {
                    _currentState = ToolState.ScaleZ;
                }
                else if (Tools.current == Tool.Rotate)
                {
                    _currentState = ToolState.RotateZ;
                }
                break;
            case KeyCode.Space:
                ApplyTranslation();
                Debug.Log("Translation applied.");
                break;
            case KeyCode.Escape:
                ResetTranslation();
                break;
            default:
                break;
        }
    }
    private static void ResetTranslation()
    {
        // Reset movement
        _mouseMoveTotal = Vector2.zero;
        MoveSelection(Vector3.zero);

        // Reset scaling
        ScaleSelection(-_scaleOffset);

        // Reset rotation
        RotateSelection(-_rotationOffset, _rotationOffsetAxis);

        // Clear offset values.
        ApplyTranslation();
    }
    private static void ApplyTranslation()
    {
        _scaleOffset = Vector3.zero;
        _rotationOffset = 0.0f;
        _rotationOffsetAxis = Vector3.zero;
        ApplySelectionMovement();
    }
    private static void MoveSelection(Vector3 movement)
    {
        _mouseMoveTotal += movement;
        for (int n = 0; n < Selection.transforms.Length; ++n)
        {
            Transform currentTransform = Selection.transforms[n];
            currentTransform.position = _selectionStartCoordinates[n] + (_mouseMoveTotal);
        }
        if (Event.current.modifiers == EventModifiers.Control)
        {
            SnapSelection(EditorPrefs.GetFloat("MoveSnapX", 0.0f),
                EditorPrefs.GetFloat("MoveSnapY", 0.0f),
                EditorPrefs.GetFloat("MoveSnapZ", 0.0f));
        }
    }
    private static void ScaleSelection(Vector3 deltaScale)
    {
        for (int n = 0; n < Selection.transforms.Length; ++n)
        {
            Transform currentTransform = Selection.transforms[n];
            currentTransform.localScale += deltaScale;
        }
        _scaleOffset += deltaScale;
    }
    private static void RotateSelection(float rotation, Vector3 axis)
    {
        for (int n = 0; n < Selection.transforms.Length; ++n)
        {
            Transform currentTransform = Selection.transforms[n];
            currentTransform.Rotate(axis, rotation);
        }
        _rotationOffsetAxis = axis;
        _rotationOffset += rotation;
    }
    private static void SnapSelection(float snapX, float snapY, float snapZ)
    {
        if (Selection.transforms.Length == 0) return;

        float xSnap;
        float ySnap;
        float zSnap;

        Transform currentTransform = Selection.transforms[0];

        xSnap = SnapValue(currentTransform.position.x, EditorPrefs.GetFloat("MoveSnapX", 0.0f));
        ySnap = SnapValue(currentTransform.position.y, EditorPrefs.GetFloat("MoveSnapY", 0.0f));
        zSnap = SnapValue(currentTransform.position.z, EditorPrefs.GetFloat("MoveSnapZ", 0.0f));

        Vector3 deltaVector = new Vector3(
            xSnap - currentTransform.position.x, 
            ySnap - currentTransform.position.y, 
            zSnap - currentTransform.position.z);

        currentTransform.position = new Vector3(xSnap, ySnap, zSnap);

        for (int n = 1; n < Selection.transforms.Length; ++n)
        {
            currentTransform = Selection.transforms[n];
            currentTransform.position += deltaVector;
        }
    }
    private static float SnapValue(float value, float snapSize)
    {
        if (snapSize <= 0.0f) return value;
        value /= snapSize;
        value = Mathf.Round(value);
        value *= snapSize;
        return value;
    }
    private static void ApplySelectionMovement()
    {
        if (_selectionStartCoordinates.Count == 0) return;
        for (int n = 0; n < Selection.transforms.Length; ++n)
        {
            _selectionStartCoordinates[n] = Selection.transforms[n].position;
        }
    }
}

﻿/**
 * Handles Demon Guy attacks.
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterController : MonoBehaviour
{
    [Header("Guns in inventory")]
    [SerializeField] private Gun[] guns;
    private Gun currentGun;

    #region public methods
    public void FireMain()
    {
        currentGun.Fire();
    }
    public void FireAlternative()
    {
        currentGun.AltFire();
    }
    public void AimStart()
    {
        currentGun.Aim(true);
    }
    public void AimEnd()
    {
        currentGun.Aim(false);
    }
    public void Reload()
    {
        currentGun.Reload();
    }
    public void SwitchAltWeaponMode()
    {
        currentGun.SwitchAltWeaponMode();
    }
    public void SwitchGuns(int gunNumber)
    {
        if (gunNumber >= guns.Length)
            gunNumber = guns.Length - 1;
        else if (gunNumber < 0)
            gunNumber = 0;

        currentGun.SwitchOut(guns[gunNumber]);
    }
    #endregion

    #region private methods
    private void Start()
    {
        currentGun = guns[0];
    }
    #endregion
}

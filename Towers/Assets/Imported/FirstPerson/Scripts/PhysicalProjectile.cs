﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicalProjectile : MonoBehaviour
{
    [Header("Projectile traveling speed and direct hit damage.")]
    [SerializeField] private float projectileSpeed = 10.0f;
    [SerializeField] private float projectileDamage = 10.0f;
    // [Header("Projectile explosion/splash on impact. Note: stacks with direct hit damage.")]
    // [SerializeField] protected float splashRadius = 0.0f;
    // [SerializeField] private float splashDamage = 0.0f;
    protected Vector3 direction = Vector3.forward;

    [Header("Projectile rigidbody and collider.")]
    [SerializeField] protected new Rigidbody rigidbody;
    [SerializeField] protected Collider projectileCollider;

    [Header("Projectile despawn time.")]
    [SerializeField] private float projectileLifetime = 5.0f;
    private float destroyTime;
    protected bool destroyOnTimer = true;
    private void OnEnable()
    {
        StartCoroutine(TimerDestroy(projectileLifetime));
    }

    // Sets transform forward to vector.
    public Vector3 ProjectileDirection
    {
        set
        {
            direction = value.normalized;
            transform.forward = direction;
            rigidbody.velocity = direction * projectileSpeed;
        }

        get
        {
            return direction;
        }
    }
    // Copies forward transform direction from target transform.
    public void SetDirection(Transform parent)
    {
        direction = (parent.up).normalized;

        if (rigidbody == null)
            Debug.Log("Error in " + gameObject.name + ": rigid body not assigned!");

        transform.forward = direction;
        rigidbody.velocity = transform.forward * projectileSpeed;

    }

    public void SetSpawnTransform(Transform parent)
    {
        transform.position = parent.position;
        SetDirection(parent);
    }

    protected virtual void OnCollisionEnter(Collision collision)
    {
        GameObject other = collision.gameObject;
        IDamageable otherDamageable = other.GetComponent<IDamageable>();
        if (otherDamageable == null)
            otherDamageable = other.GetComponentInParent<IDamageable>();
        if (otherDamageable != null)
        {
            otherDamageable.TakeDamage(ProjectileDamage);
            otherDamageable.TakeImpact(collision.contacts[0].point, 
                collision.relativeVelocity);
            Destroy(this.gameObject);
            // Debug.Log(this.name + " damaging " + other.name);
        }
        else
        {
            this.enabled = false;
        }
    }

    IEnumerator TimerDestroy(float lifeTime)
    {
        yield return new WaitForSeconds(lifeTime);
        {
            if (destroyOnTimer)
                Destroy(gameObject);
            StopCoroutine(TimerDestroy(0.0f));
        }
        yield return null;
    }

    public float ProjectileDamage
    {
        get
        {
            return projectileDamage;
        }
    }
}

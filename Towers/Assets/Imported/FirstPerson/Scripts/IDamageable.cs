﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageable
{

    void TakeDamage(float damageAmount);
    void TakeImpact(Vector3 impactPosition, Vector3 impactImpulse);
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for guns, applicable to any object that fires projectiles.
/// </summary>
public class Gun : MonoBehaviour
{
    public virtual void Fire() { }
    public virtual void AltFire() { }
    public virtual void Aim(bool aimNow) { }
    public virtual void Reload() { }
    public virtual void SwitchAltWeaponMode() { }
    public virtual void SwitchOut(Gun switchToGun) { }
    public virtual void SwitchIn() { }
}

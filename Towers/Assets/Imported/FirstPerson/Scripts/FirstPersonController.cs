﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonController : MonoBehaviour
{
    // Variables relating to control and input.
    [Header("Input Settings")]
    [SerializeField] private float movementSpeed = 6.0f;
    [SerializeField] private float jumpSpeed = 50.0f;
    [SerializeField] private float gravityAcceleration = 20.0f;
    [SerializeField] private float lookXSensitivity = 1.0f;
    [SerializeField] private float lookYSensitivity = 1.0f;
    [SerializeField] private float momentumBuild = 24.0f;
    [SerializeField] private float momentumDrag = 24.0f;

    private Vector3 currentMomentum = Vector3.zero;
    public enum state
    {
        sprinting,
        standing,
        walking,
        running,
        jumping,
        falling
    };
    private state currentState = state.running;
    // References
    [Header("References to body parts")]
    [SerializeField] private Transform headTransform;
    [SerializeField] private CharacterController bodyController;

    // [SerializeField] private DemonGuyAnimation animationScript;
    // [SerializeField] private Transform thirdPersonBase;

    // Use this for initialization
    void Start()
    {
        if (bodyController == null || headTransform == null)
        {
            throw new System.Exception("Error in " + name +
                ": need to assign head transform and body controller.");
        }
    }

    /// <summary>
    /// Moves transform using charactercontroller.
    /// </summary>
    public void Move(Vector3 moveDirection)
    {
        // TODO: Change to use input. Allow disabling sprinting.
        float moveVertical = moveDirection.y;
        float groundDragFactor = 1.0f;
        float groundMoveFactor = 1.0f;
        float maxSpeedFactor = 1.0f;
        // Check if airborne or not. Determines movement style.
        if (!bodyController.isGrounded)
        {
            groundDragFactor = 0.1f; // Reduce drag while airborne.
            groundMoveFactor = 0.5f;
            maxSpeedFactor = 1.5f;
        }
        #region Apply movement controls.
        {
            moveDirection = bodyController.transform.TransformDirection(moveDirection);
            moveDirection = Vector3.ProjectOnPlane(moveDirection, Vector3.up);
            moveDirection.Normalize();

            // Determine state.
            if (bodyController.isGrounded)
            {
                currentMomentum.y = 0.0f;
                // Sprint.
                if (currentState == state.sprinting)
                {
                    currentState = state.sprinting;
                    groundMoveFactor = 1.5f;
                    maxSpeedFactor = 1.5f;
                }
                // Walk.
                else if (currentState == state.walking)
                {
                    groundMoveFactor = 0.5f;
                }
            }
            // Build momentum.
            moveDirection.x *= groundMoveFactor;
            moveDirection.z *= groundMoveFactor;
            moveDirection.x *= momentumBuild;
            moveDirection.z *= momentumBuild;
            currentMomentum += moveDirection * Time.deltaTime;
        }
        #endregion

        #region Apply drag, limit max velocity.
        {
            Vector3 horizontalMomentum = new Vector3(currentMomentum.x, 0, currentMomentum.z);
            // Limit maximum movement speed.
            if (horizontalMomentum.magnitude >= movementSpeed * maxSpeedFactor)
            {
                horizontalMomentum = horizontalMomentum.normalized * movementSpeed * maxSpeedFactor;
                currentMomentum.x = horizontalMomentum.x;
                currentMomentum.z = horizontalMomentum.z;
            }
            // Apply momentum friction.
            if (horizontalMomentum != Vector3.zero && moveDirection == Vector3.zero)
            {
                Vector3 dragVector = -horizontalMomentum.normalized * Time.deltaTime *
                    momentumDrag * groundDragFactor;
                if (horizontalMomentum.magnitude - dragVector.magnitude < 0f)
                {
                    currentMomentum.x = 0.0f;
                    currentMomentum.z = 0.0f;
                    currentState = state.standing;
                    // Debug.Log("Momentum stopped due to drag.");
                }
                else
                    currentMomentum += dragVector;
            }
            currentMomentum.y += moveVertical;
            currentMomentum.y -= gravityAcceleration * Time.deltaTime;
        }
        #endregion
        // Apply movement.
        bodyController.Move(currentMomentum * Time.deltaTime);
    }

    public void Jump(float jumpForce = -1)
    {
        if (!bodyController.isGrounded)
            return;

        if (jumpForce == -1)
            jumpForce = jumpSpeed;
        currentState = state.jumping;
        Move(new Vector3(0.0f, jumpForce, 0.0f));
    }

    /// <summary>
    /// Look around. Rotates transform of head.
    /// </summary>
    public void Look(Vector2 lookDelta)
    {
        // Convert input to degrees of rotation and apply sensitivity settings.
        float deltaLookHor = lookDelta.x * 360 * lookXSensitivity;
        float deltaLookVert = lookDelta.y * 360 * lookYSensitivity;

        // Rotate body according to look horizontal rotation.
        bodyController.transform.Rotate(new Vector3(0, deltaLookHor, 0), Space.World);

        // Rotate head (the camera's root) accoring to look vertical rotation.
        headTransform.Rotate(new Vector3(-deltaLookVert, 0, 0), Space.Self);

        // Prevent looking at a negative angle.
        Vector3 newForward = transform.forward;
        float angleUpOverload = Vector3.SignedAngle(headTransform.forward,
            Vector3.up, headTransform.right);
        if (angleUpOverload > 0)
        {
            if (angleUpOverload < 90)
                headTransform.Rotate(angleUpOverload, 0, 0);
            else
            {
                Quaternion rotation = new Quaternion();
                rotation = Quaternion.Euler(-180 + angleUpOverload, 0, 0);
                headTransform.rotation *= rotation;
            }
        }
    }
    // GET / SET
    public float CurrentSpeed
    {
        get
        {
            if (movementSpeed != 0f)
                return (currentMomentum - new Vector3(0, currentMomentum.y)).magnitude / movementSpeed;
            else
                return 0f;
        }
    }

    public float CurrentTurnAngle
    {
        get
        {
            return 0.0f;
        }
    }

    public bool OnGround
    {
        get
        {
            return (bodyController.isGrounded);
        }
    }
    public state CurrentState
    {
        get
        {
            return currentState;
        }
        set
        {
            currentState = value;
        }
    }

}

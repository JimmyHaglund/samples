﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Input for shooter controller script using Unity'd default input system.
/// </summary>
public class ShooterPlayerInputDefault : MonoBehaviour
{
    [Header("Base input names")]
    [SerializeField] private string fireButton = "";
    [Tooltip("Set to blank to have no alt fire mode.")]
    [SerializeField] private string alternativeFireButton = "";
    [SerializeField] private string reloadButton = "";
    [SerializeField] private string alternativeWeaponModeSwitchButton = "";
    [SerializeField] private string aimDownSightsButton = "";
    
    [Header("Weapon quick switch keys")]
    [SerializeField] private string[] weaponSwitchButtons;

    [Space]
    [SerializeField] private ShooterController shooterController;

    void Start()
    {
        // Write out un - set buttons at initialization.
        if (fireButton == "")
            Debug.Log(this + ": Fire Button is not set.");
        if (alternativeFireButton == "")
            Debug.Log(this + ": Alternative Fire Button is not set.");
        if (reloadButton == "")
            Debug.Log(this + ": Reload Button is not set.");
        if (alternativeWeaponModeSwitchButton == "")
            Debug.Log(this + ": Alternative Weapon Mode Switch Button is not set.");
        if (aimDownSightsButton == "")
            Debug.Log(this + ": Aim Down Sights Button is not set.");
    }

    void Update()
    {
        // Fire.
        if (fireButton != "" 
            && Input.GetButton(fireButton))
        {
            shooterController.FireMain();
        }
        // Alternative fire.
        else if (alternativeFireButton != ""
            && Input.GetButton(alternativeFireButton))
        {
            shooterController.FireAlternative();
        }
        // Aim down sights.
        if (aimDownSightsButton != "" 
            && Input.GetButton(aimDownSightsButton))
        {
            shooterController.AimStart();
        }
        else
        {
            shooterController.AimEnd();
        }
        // Reload.
        if (reloadButton != "" 
            && Input.GetButtonDown(reloadButton))
        {
            shooterController.Reload();
        }
        // Switch to alternative weapon mode.
        if (alternativeWeaponModeSwitchButton != ""
            && Input.GetButtonDown(alternativeWeaponModeSwitchButton))
        {
            shooterController.SwitchAltWeaponMode();
        }
        // Switch weapon.
        for (int n = 0; n < weaponSwitchButtons.Length; ++n)
        {
            if (Input.GetButtonDown(weaponSwitchButtons[n]))
            {
                shooterController.SwitchGuns(n);
                break;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Uses Unity's built-in input manager to accept input and transfer it to the First Person Controller.
/// </summary>
public class FirstPersonInputDefault : MonoBehaviour
{
    [Header("Input manager button & axis names")]
    [SerializeField] private string moveForwardAndBackwardAxis = "Vertical";
    [SerializeField] private string strafeLeftAndRightAxis = "Horizontal";
    [SerializeField] private string jumpButton = "Jump";
    [Tooltip("Button to walk. Set blank to have no binding.")]
    [SerializeField] private string walkButton = "";
    [Tooltip("Button to sprint. Set blank to have no binding.")]
    [SerializeField] private string sprintButton = "";
    [SerializeField] private string lookVerticalAxis = "Mouse Y";
    [SerializeField] private string lookHorizontalAxis = "Mouse X";
    [Space]
    [SerializeField] private FirstPersonController controller;


    // Use this for initialization
    private void Start()
    {
        if (walkButton == "")
            Debug.Log(this + ": walk button is blank.");
        if (sprintButton == "")
            Debug.Log(this + ": sprint button is blank.");
        if (controller == null)
        {
            throw new System.Exception("Error in " + name +
                ": need to assign a FirstPersonController script.");
        }
        SetCursorState(CursorLockMode.Locked);
    }

    // Update is called once per frame
    private void Update()
    {
        // Check for looking around.
        Vector2 lookVector = new Vector2(
            Input.GetAxis(lookHorizontalAxis) / 100,
            Input.GetAxis(lookVerticalAxis) / 100);
        controller.Look(lookVector);

        // Check jumping.
        if (Input.GetButton(jumpButton))
        {
            controller.Jump();
        }

        // Check walk / sprint modifiers.
        // TODO: Allow setting walk / sprint to enabled/disabled and toggle/hold.
        if (walkButton != "")
        {
            if(Input.GetButtonDown(walkButton) || Input.GetAxis(moveForwardAndBackwardAxis) < 0.4f)
            {
                controller.CurrentState = FirstPersonController.state.walking;
            }
            else if (Input.GetButtonUp(walkButton)
                && controller.CurrentState == FirstPersonController.state.walking)
            {
                controller.CurrentState = FirstPersonController.state.running;
            }
        }

        if (sprintButton != "")
        {
            if (Input.GetButtonDown(sprintButton))
            {
                controller.CurrentState = FirstPersonController.state.sprinting;
            }
            else if (Input.GetButtonUp(sprintButton)
                && controller.CurrentState == FirstPersonController.state.sprinting)
            {
                controller.CurrentState = FirstPersonController.state.running;
            }
        }

        // Check movement.
        Vector3 movementInput = new Vector3(
            Input.GetAxis(strafeLeftAndRightAxis),
            0.0f,
            Input.GetAxis(moveForwardAndBackwardAxis));
        controller.Move(movementInput);
    }

    private void SetCursorState(CursorLockMode wantedMode)
    {
        Cursor.lockState = wantedMode;
        Cursor.visible = (Cursor.lockState != CursorLockMode.Locked);
    }

    public void UnLockCursor()
    {
        SetCursorState(CursorLockMode.None);
    }

    public void LockCursor()
    {
        SetCursorState(CursorLockMode.Locked);
    }
}

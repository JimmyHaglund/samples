﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoltProjectile : PhysicalProjectile
{
    
    protected override void OnCollisionEnter(Collision collision)
    {
        GameObject other = collision.gameObject;
        IDamageable otherDamageable = other.GetComponent<IDamageable>();
        if (otherDamageable == null)
            otherDamageable = other.GetComponentInParent<IDamageable>();
        if (otherDamageable != null)
        {
            otherDamageable.TakeDamage((int)ProjectileDamage);
            otherDamageable.TakeImpact(collision.contacts[0].point,
                collision.relativeVelocity);
            // Debug.Log(name + " colliding with " + other.name);
            Object.Destroy(rigidbody);
            projectileCollider.enabled = false;
            destroyOnTimer = false;
            transform.position += transform.forward * (0.1f * Random.Range(0.5f, 2f));
            this.transform.parent = other.transform;
            this.gameObject.layer = other.layer;
        }
        else
            this.enabled = false;
    }
}

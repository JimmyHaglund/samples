﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DemonGunController : Gun
{
    [Header("Gun settings")]
    [SerializeField]
    private Transform bulletPoint;
    [SerializeField] private GameObject shotGunShrapnelObject;
    [SerializeField] private int shotgunAmmo = 6;
    [Tooltip("The angle of the shotgun's projectiles spread cone in degrees.")]
    [SerializeField] private float shotGunSpreadConeAngle = 60;
    [SerializeField] private int shrapnelAmount = 50;
    [SerializeField] private GameObject boltObject;
    [SerializeField] private int boltGunAmmo = 20;

    [Header("Animation settings")]
    [SerializeField] private Animator gunAnimator;
    [SerializeField] private float fireShotgunSpeedFactor = 1.0f;
    [SerializeField] private float fireBoltgunSpeedFactor = 1.0f;
    [SerializeField] private Animator shotGunCylinderAnimator;
    [SerializeField] private float switchGunSpeedFactor = 1.0f;

    [Header("UI settings")]
    [SerializeField] private Text shotgunAmmoText;
    [SerializeField] private Text boltgunAmmoText;

    private int maxShotgunAmmo;
    private int maxBoltgunAmmo;

    void Start()
    {
        maxShotgunAmmo = shotgunAmmo;
        maxBoltgunAmmo = boltGunAmmo;
        BoltGunAmmo = boltGunAmmo;
        ShotGunAmmo = shotgunAmmo;

        gunAnimator.SetFloat("ShotgunSpeed", fireShotgunSpeedFactor);
        gunAnimator.SetFloat("BoltgunSpeed", fireBoltgunSpeedFactor);
        shotGunCylinderAnimator.SetFloat("SwitchSpeed", switchGunSpeedFactor);
    }

    private enum WeaponMode
    {
        shotGun,
        boltGun
    };
    private WeaponMode currentMode = WeaponMode.shotGun;

    void Update()
    {

    }

    public override void Fire()
    {
        if (currentMode == WeaponMode.shotGun)
        {
            if (shotGunCylinderAnimator.GetCurrentAnimatorStateInfo(0).IsName("IdleShotgun") &&
            !gunAnimator.GetCurrentAnimatorStateInfo(0).IsName("Base.ShotgunRecoil"))
                FireShotgun();
        }
        else
        {
            if (shotGunCylinderAnimator.GetCurrentAnimatorStateInfo(0).IsName("IdleBoltgun") &&
            !gunAnimator.GetCurrentAnimatorStateInfo(0).IsName("Base.BoltgunRecoil"))
                FireBoltGun();
        }
    }
    public override void Aim(bool aimNow)
    {

    }
    public override void Reload()
    {

    }
    public override void SwitchAltWeaponMode()
    {
        if (currentMode == WeaponMode.shotGun)
        {
            if (shotGunCylinderAnimator.GetCurrentAnimatorStateInfo(0).IsName("IdleShotgun") &&
            !gunAnimator.GetCurrentAnimatorStateInfo(0).IsName("Base.ShotgunRecoil"))
            {
                shotGunCylinderAnimator.Play("Base.ChangeToBoltgun");
                currentMode = WeaponMode.boltGun;
            }
        }
        else
        {
            if (shotGunCylinderAnimator.GetCurrentAnimatorStateInfo(0).IsName("IdleBoltgun") &&
            !gunAnimator.GetCurrentAnimatorStateInfo(0).IsName("Base.BoltgunRecoil"))
            {
                shotGunCylinderAnimator.Play("Base.ChangeToShotgun");
                currentMode = WeaponMode.shotGun;
            }
                
        }
    }
    public override void SwitchOut(Gun switchToGun)
    {
        switchToGun.SwitchIn();
    }
    public override void SwitchIn()
    {
        
    }
    public override void AltFire()
    {

    }

    private void FireShotgun()
    {
        if (shotgunAmmo <= 0)
            return;
        gunAnimator.Play("Base.ShotgunRecoil");
        ShotGunAmmo--;
        for (int n = 0; n < shrapnelAmount; ++n)
        {
            GameObject newObject;
            PhysicalProjectile newProjectile;

            // lastFireTime = Time.time;
            newObject = GameObject.Instantiate(shotGunShrapnelObject);
            newProjectile = newObject.GetComponent<PhysicalProjectile>();

            newProjectile.transform.position = bulletPoint.position;
            newProjectile.SetSpawnTransform(bulletPoint);
            float ang = shotGunSpreadConeAngle / 2;

            Quaternion randomDir = Quaternion.Euler(
                Random.Range(-ang, ang),
                Random.Range(-ang, ang),
                Random.Range(-ang, ang));
            newProjectile.ProjectileDirection = (randomDir * newProjectile.ProjectileDirection);
            newProjectile.transform.position += newProjectile.ProjectileDirection * 0.1f;
        }
    }

    private void FireBoltGun()
    {
        if (boltGunAmmo <= 0)
            return;

        gunAnimator.Play("Base.BoltgunRecoil");
        BoltGunAmmo--;

        GameObject newObject;
        PhysicalProjectile newProjectile;

        // lastFireTime = Time.time;
        newObject = GameObject.Instantiate(boltObject);
        newProjectile = newObject.GetComponent<PhysicalProjectile>();

        newProjectile.transform.position = bulletPoint.position;
        newProjectile.SetSpawnTransform(bulletPoint);
    }

    public int BoltGunAmmo
    {
        set
        {
            boltGunAmmo = value;
            if (boltGunAmmo > maxBoltgunAmmo)
                boltGunAmmo = maxBoltgunAmmo;
            else if (boltGunAmmo < 0)
                boltGunAmmo = 0;

            if (boltgunAmmoText != null)
            {
                boltgunAmmoText.text = boltGunAmmo.ToString();
            }
        }
        get
        {
            return boltGunAmmo;
        }
    }

    public int ShotGunAmmo
    {
        set
        {
            shotgunAmmo = value;
            if (shotgunAmmo > maxShotgunAmmo)
                shotgunAmmo = maxShotgunAmmo;
            else if (shotgunAmmo < 0)
                shotgunAmmo = 0;

            if (shotgunAmmoText != null)
            {
                shotgunAmmoText.text = shotgunAmmo.ToString();
            }
        }
        get
        {
            return shotgunAmmo;
        }
    }

    public void AddBoltgunAmmo(int amount)
    {
        BoltGunAmmo += amount;
    }

    public void AddShotgunAmmo(int amount)
    {
        ShotGunAmmo += amount;
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
// TODO: Slider for text size.
// TODO: Slider for button size.
[ExecuteInEditMode]
public class InterfaceSettings : MonoBehaviour
{
    [Header("Header text settings")]
    [SerializeField] private Text[] headers;
    [SerializeField] private Font headerFont;
    [SerializeField] private Color headerColor;

    [Header("Button settings")]
    [SerializeField] private Image[] mainMenuButtons;
    [SerializeField] private Image[] otherButtons;
    [SerializeField] private Color buttonColor;
    [SerializeField] private Font buttonTextFont;
    [SerializeField] private Color buttonTextColor;

    [Header("Menu background settings")]
    [SerializeField] private Image[] backgrounds;
    [SerializeField] private Color backgroundColor;
    [SerializeField] private Color backgroundOutlineColor;
    [SerializeField] private Font textFont;
    [SerializeField] private Color textColor;
    // TODO: Slider for changing button spacing. -> Set specifically for main menu buttons.

    // Update is called once per frame
    void Update()
    {
        // Header settings
        {
            for (int n = 0; n < headers.Length; ++n)
            {
                headers[n].font = headerFont;
                headers[n].color = headerColor;
            }
        }
        // Buttons settings
        {
            Text buttonText;
            for (int n = 0; n < mainMenuButtons.Length; ++n)
            {
                mainMenuButtons[n].color = buttonColor;
                buttonText = mainMenuButtons[n].GetComponentInChildren<Text>();
                buttonText.font = buttonTextFont;
                buttonText.color = buttonTextColor;
                // TODO: Insert code for button spacing here.
            }
            for (int i = 0; i < otherButtons.Length; ++i)
            {
                otherButtons[i].color = buttonColor;
                buttonText = otherButtons[i].GetComponentInChildren<Text>();
                buttonText.font = buttonTextFont;
                buttonText.color = buttonTextColor;
            }
        }
        // Background & text settings
        {
            Text[] texts;
            Outline backgroundOutline;
            for (int n = 0; n < backgrounds.Length; ++n)
            {
                backgrounds[n].color = backgroundColor;

                backgroundOutline = backgrounds[n].GetComponent<Outline>();
                if (backgroundOutline != null)
                    backgroundOutline.effectColor = backgroundOutlineColor;

                texts = backgrounds[n].GetComponentsInChildren<Text>();
                for (int i = 0; i < texts.Length; ++i)
                {
                    texts[i].font = textFont;
                    texts[i].color = textColor;
                }
            }
        }
    }
}

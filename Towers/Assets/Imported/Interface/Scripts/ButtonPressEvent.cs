﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ButtonPressEvent : MonoBehaviour
{
    [SerializeField] private KeyCode[] keys;
    [SerializeField] private string[] buttons;
    [SerializeField] private UnityEvent onPressEvent;
    

    // Update is called once per frame
    void Update()
    {
        for (int n = 0; n < keys.Length; ++n)
        {
            if (Input.GetKeyDown(keys[n]))
            {
                ActivateEvent();
                return;
            }
        }
        for (int i = 0; i < buttons.Length; ++i)
        {
            if (Input.GetButtonDown(buttons[i]))
            {
                ActivateEvent();
                return;
            }
        }
    }
    private void ActivateEvent()
    {
        onPressEvent.Invoke();
    }
    public void InvokeExternally()
    {
        ActivateEvent();
    }
}

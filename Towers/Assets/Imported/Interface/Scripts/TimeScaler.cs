﻿/**
 * Used to adjust time scale via events.
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeScaler : MonoBehaviour
{
    [SerializeField] private Text timeScaleDisplayText;

    public void SetTimeScale(float timeScale)
    {
        Time.timeScale = timeScale;
    }

    public float GetTimeScale()
    {
        return Time.timeScale;
    }

    private void Update()
    {
        if (timeScaleDisplayText != null)
            timeScaleDisplayText.text = "Time scale: " + Time.timeScale.ToString();
    }
}

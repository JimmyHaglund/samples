﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestroy : MonoBehaviour
{
    [SerializeField] private float destroyTimer = 2.0f;
    // Use this for initialization
    void Start()
    {
        StartCoroutine(DestroySelf(destroyTimer));
    }

    private IEnumerator DestroySelf(float timer)
    {
        yield return new WaitForSeconds(timer);
        Destroy(gameObject);
        yield return null;
    }
}

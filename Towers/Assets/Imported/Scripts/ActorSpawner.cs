﻿/**
 * The actor spawner class allows new actors to be created from prefabs.
 * It can be used to fire bullets, or place objects in game.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActorSpawner : MonoBehaviour
{
    [SerializeField] private List<GameObject> spawnObject;
    public bool SpawnActor(int actorIndex, Vector3 origin, Vector3 spawnFaceDirection, float spawnSpeed = 0)
    {
        if (spawnObject.Count == 0)
            return false;
        if (spawnObject.Count <= actorIndex)
            actorIndex = spawnObject.Count - 1;
        GameObject newInstance = Instantiate(spawnObject[actorIndex], null);
        newInstance.transform.position = origin;
        newInstance.transform.LookAt(origin + spawnFaceDirection);
        newInstance.GetComponent<Rigidbody>().velocity = newInstance.transform.forward * spawnSpeed;
        return true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utility : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// Compares an array of ray cast hit
    /// objects and returns the one with
    /// the closest distance to a given
    /// point.
    /// </summary>
    /// <param name="origin"></param>
    /// <param name="rayHitArray"></param>
    /// <returns></returns>
    RaycastHit GetClosestPointOfRayHits(Vector3 origin, RaycastHit[] rayHitArray)
    {
        float shortestDistance = Mathf.Infinity;
        RaycastHit closestHit = new RaycastHit();
        for (int n = 0; n < rayHitArray.Length; ++n)
        {
            if (rayHitArray[n].collider == null)
                continue;
            float distance = (rayHitArray[n].point - origin).sqrMagnitude;
            if (distance < shortestDistance)
            {
                shortestDistance = distance;
                closestHit = rayHitArray[n];
            }
        }
        return closestHit;
    }
}

﻿/**
 * Allows a trigger collider object to activate other objects when entered by the player.
 * 
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivationTrigger : MonoBehaviour
{
    [SerializeField] private GameObject[] activatedObjects;

    // Deactivate objects on start.
    private void Start()
    {
        for (int n = 0; n < activatedObjects.Length; ++n)
        {
            activatedObjects[n].SetActive(false);
        }
    }
    // Activate objects when triggered.
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Player")
            return;
        for (int n = 0; n < activatedObjects.Length; ++n)
        {
            activatedObjects[n].SetActive(true);
        }
        Destroy(this);
    }
}

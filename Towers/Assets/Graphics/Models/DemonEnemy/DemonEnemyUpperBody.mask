%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: DemonEnemyUpperBody
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 0
  - m_Path: Armature/ArmIKRoot.L
    m_Weight: 0
  - m_Path: Armature/ArmIKRoot.L/ArmIKRoot.L_end
    m_Weight: 0
  - m_Path: Armature/ArmIKRoot.R
    m_Weight: 0
  - m_Path: Armature/ArmIKRoot.R/ArmIKRoot.R_end
    m_Weight: 0
  - m_Path: Armature/ElbowIKTarget.L
    m_Weight: 0
  - m_Path: Armature/ElbowIKTarget.L/ElbowIKTarget.L_end
    m_Weight: 0
  - m_Path: Armature/ElbowIKTarget.R
    m_Weight: 0
  - m_Path: Armature/ElbowIKTarget.R/ElbowIKTarget.R_end
    m_Weight: 0
  - m_Path: Armature/Eye.L
    m_Weight: 0
  - m_Path: Armature/KneeIKTarget.L
    m_Weight: 0
  - m_Path: Armature/KneeIKTarget.L/KneeIKTarget.L_end
    m_Weight: 0
  - m_Path: Armature/KneeIKTarget.R
    m_Weight: 0
  - m_Path: Armature/KneeIKTarget.R/KneeIKTarget.R_end
    m_Weight: 0
  - m_Path: Armature/LegIKRoot.L
    m_Weight: 0
  - m_Path: Armature/LegIKRoot.L/LegIKRoot.L_end
    m_Weight: 0
  - m_Path: Armature/LegIKRoot.R
    m_Weight: 0
  - m_Path: Armature/LegIKRoot.R/LegIKRoot.R_end
    m_Weight: 0
  - m_Path: Armature/Pelvis
    m_Weight: 0
  - m_Path: Armature/Pelvis/Spine.001
    m_Weight: 0
  - m_Path: Armature/Pelvis/Spine.001/Spine.002
    m_Weight: 1
  - m_Path: Armature/Pelvis/Spine.001/Spine.002/Spine.003
    m_Weight: 1
  - m_Path: Armature/Pelvis/Spine.001/Spine.002/Spine.003/Spine.004
    m_Weight: 1
  - m_Path: Armature/Pelvis/Spine.001/Spine.002/Spine.003/Spine.004/Clavicle.L
    m_Weight: 1
  - m_Path: Armature/Pelvis/Spine.001/Spine.002/Spine.003/Spine.004/Clavicle.L/UpperArm.L
    m_Weight: 1
  - m_Path: Armature/Pelvis/Spine.001/Spine.002/Spine.003/Spine.004/Clavicle.L/UpperArm.L/ForeArm.L
    m_Weight: 1
  - m_Path: Armature/Pelvis/Spine.001/Spine.002/Spine.003/Spine.004/Clavicle.L/UpperArm.L/ForeArm.L/Hand.L
    m_Weight: 1
  - m_Path: Armature/Pelvis/Spine.001/Spine.002/Spine.003/Spine.004/Clavicle.L/UpperArm.L/ForeArm.L/Hand.L/Hand.L_end
    m_Weight: 1
  - m_Path: Armature/Pelvis/Spine.001/Spine.002/Spine.003/Spine.004/Clavicle.R
    m_Weight: 1
  - m_Path: Armature/Pelvis/Spine.001/Spine.002/Spine.003/Spine.004/Clavicle.R/UpperArm.R
    m_Weight: 1
  - m_Path: Armature/Pelvis/Spine.001/Spine.002/Spine.003/Spine.004/Clavicle.R/UpperArm.R/ForeArm.R
    m_Weight: 1
  - m_Path: Armature/Pelvis/Spine.001/Spine.002/Spine.003/Spine.004/Clavicle.R/UpperArm.R/ForeArm.R/Hand.R
    m_Weight: 1
  - m_Path: Armature/Pelvis/Spine.001/Spine.002/Spine.003/Spine.004/Clavicle.R/UpperArm.R/ForeArm.R/Hand.R/Hand.R_end
    m_Weight: 1
  - m_Path: Armature/Pelvis/Spine.001/Spine.002/Spine.003/Spine.004/Neck
    m_Weight: 1
  - m_Path: Armature/Pelvis/Spine.001/Spine.002/Spine.003/Spine.004/Neck/Head
    m_Weight: 1
  - m_Path: Armature/Pelvis/Spine.001/Spine.002/Spine.003/Spine.004/Neck/Head/Head_end
    m_Weight: 1
  - m_Path: Armature/Pelvis/Spine.001/Thigh.L
    m_Weight: 0
  - m_Path: Armature/Pelvis/Spine.001/Thigh.L/Calf.L
    m_Weight: 0
  - m_Path: Armature/Pelvis/Spine.001/Thigh.L/Calf.L/Foot.L
    m_Weight: 0
  - m_Path: Armature/Pelvis/Spine.001/Thigh.L/Calf.L/Foot.L/Foot.L_end
    m_Weight: 0
  - m_Path: Armature/Pelvis/Spine.001/Thigh.R
    m_Weight: 0
  - m_Path: Armature/Pelvis/Spine.001/Thigh.R/Calf.R
    m_Weight: 0
  - m_Path: Armature/Pelvis/Spine.001/Thigh.R/Calf.R/Foot.R
    m_Weight: 0
  - m_Path: Armature/Pelvis/Spine.001/Thigh.R/Calf.R/Foot.R/Foot.R_end
    m_Weight: 0
  - m_Path: Armature/SpineIKRoot
    m_Weight: 0
  - m_Path: Armature/SpineIKRoot/SpineIKRoot_end
    m_Weight: 0
  - m_Path: Armature/SpineIKTarget
    m_Weight: 0
  - m_Path: Armature/SpineIKTarget/SpineIKTarget_end
    m_Weight: 0
  - m_Path: Eye.R
    m_Weight: 0
  - m_Path: LowDetailModel
    m_Weight: 0
  - m_Path: Tooth
    m_Weight: 0
  - m_Path: Tooth.001
    m_Weight: 0
  - m_Path: Tooth.002
    m_Weight: 0
  - m_Path: Tooth.003
    m_Weight: 0
  - m_Path: Tooth.004
    m_Weight: 0
  - m_Path: Tooth.005
    m_Weight: 0
  - m_Path: Tooth.006
    m_Weight: 0
  - m_Path: Tooth.007
    m_Weight: 0
  - m_Path: Tooth.008
    m_Weight: 0
  - m_Path: Tooth.009
    m_Weight: 0
  - m_Path: Tooth.010
    m_Weight: 0
  - m_Path: Tooth.011
    m_Weight: 0

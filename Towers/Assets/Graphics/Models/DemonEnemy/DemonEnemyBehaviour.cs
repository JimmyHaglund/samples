﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimation : MonoBehaviour
{
    [SerializeField] private Animator myAnimator;
    [SerializeField][Range(0.0f, 1.0f)] private float turn;
    [SerializeField][Range(0.0f, 1.0f)] private float movement;


    // Update is called once per frame
    void Update()
    {
        myAnimator.SetFloat("Movement", movement);
        myAnimator.SetFloat("Turn", turn);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour, IDamageable
{
    [SerializeField] private float healthAmount = 1000f;
    [SerializeField] private UnityEvent onDeathEvent;
    [SerializeField] private Slider healthBarSlider;
    private float maxHealth;

    void Start()
    {
        MaxHealth = healthAmount;
        HealthAmount = healthAmount;
    }

    public void TakeDamage(float amount)
    {
        HealthAmount -= amount;
    }

    public void TakeImpact(Vector3 position, Vector3 direction)
    {

    }

    private float HealthAmount
    {
        set
        {
            healthAmount = value;
            if (healthAmount < 0.0f)
                healthAmount = 0.0f;
            else if (healthAmount > maxHealth)
                healthAmount = maxHealth;
            if (healthBarSlider != null)
                healthBarSlider.value = healthAmount;

            if (healthAmount == 0.0f)
            {
                onDeathEvent.Invoke();
            }

        }
        get
        {
            return healthAmount;
        }
    }

    private float MaxHealth
    {
        set
        {
            maxHealth = value;
            if (healthBarSlider != null)
                healthBarSlider.maxValue = maxHealth;
        }
        get
        {
            return maxHealth;
        }
    }
}

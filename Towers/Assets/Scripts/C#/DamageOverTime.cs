﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Causes damage to entity entering one's trigger.
/// </summary>
public class DamageOverTime : MonoBehaviour
{
    [SerializeField] private float damagePerTick = 5.0f;
    [SerializeField] private float tickTime = 1.0f;
    [SerializeField] private PlayerHealth playerHealthScript;

    private bool damageTimerActive = false;
    private void OnTriggerEnter(Collider other)
    {
        IDamageable otherHealth = other.GetComponent<IDamageable>();
        // Prevent anything but player from being applied.
        if (otherHealth != playerHealthScript.GetComponent<IDamageable>())
            return;
        if (otherHealth != null)
        {
            damageTimerActive = true;
            StartCoroutine(DamageTick(otherHealth));
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<IDamageable>() == playerHealthScript.GetComponent<IDamageable>())
        {
            damageTimerActive = false;
            StopAllCoroutines();
        }
    }

    /// <summary>
    /// Cause damage after a set amount of time to target entity.
    /// </summary>
    /// <param name="damagedEntity"></param>
    /// <returns></returns>
    private IEnumerator DamageTick(IDamageable damagedEntity)
    {
        yield return new WaitForSeconds(tickTime);
        if (damageTimerActive)
        {
            damagedEntity.TakeDamage(damagePerTick);
            StartCoroutine(DamageTick(damagedEntity));
        }
        yield return null;
    }
}

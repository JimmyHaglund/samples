﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class EnemyCounter : MonoBehaviour
{
    [Tooltip("Number of enemies to defeat before event is run.")]
    [SerializeField] private int enemyCount = 0;
    [SerializeField] private UnityEvent onEnemiesClearedEvent;
    
    public void ReduceEnemyCount()
    {
        if (--enemyCount <= 0)
        {
            onEnemiesClearedEvent.Invoke();
            // Debug.Log("Enemy count 0.");
            // Destroy(this);
        }
    }
}

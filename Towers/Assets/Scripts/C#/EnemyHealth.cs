﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemyHealth : MonoBehaviour, IDamageable
{
    private float health;
    [SerializeField] private float maxHealth = 100;
    [SerializeField] private GameObject parentObject;
    [SerializeField] private GameObject impactEffect;
    [SerializeField] private GameObject onDeathEffect;
    [SerializeField] private UnityEvent onDeathEvent;

    private bool isDead = false;
    
    /// <summary>
    /// Takes damage.
    /// </summary>
    /// <param name="damageAmount"></param>
    public void TakeDamage(float damageAmount)
    {
        Health -= damageAmount;
    }
    private float Health
    {
        set
        {
            health = value;

            if (health <= 0)
                Die();
            else if (health > maxHealth)
                health = maxHealth;
            // Debug.Log(this + " took damage. Health: " + health);
        }

        get
        {
            return health;
        }
    }
    /// <summary>
    /// Is run by sending a message from an object impacting this one. 
    /// Sends out particle effects and plays audio relative to where the impact happened.
    /// </summary>
    /// <param name="impactPosition"></param>
    /// <param name="impactDirection"></param>
    public void TakeImpact(Vector3 impactPosition, Vector3 impactDirection)
    {
        // Debug.Log(this + " took impact at point: " + impactPosition + 
        //     " and direction: " + impactDirection + ".");
        GameObject newCube = GameObject.Instantiate(impactEffect, transform);
        newCube.transform.position = impactPosition;
        newCube.transform.forward = impactDirection;
       
    }
    /// <summary>
    /// Kills unit.
    /// </summary>
    private void Die()
    {
        if (isDead)
            return;
        isDead = true;
        GameObject deathEffect = GameObject.Instantiate(onDeathEffect);
        deathEffect.transform.position = transform.position;
        onDeathEvent.Invoke();
        Destroy(parentObject);
    }
    private void Start()
    {
        Health = maxHealth;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateConstant : MonoBehaviour
{
    [SerializeField] Vector3 rotationVelocity;

	void Update ()
    {
        transform.Rotate(rotationVelocity*Time.deltaTime);	
	}
}

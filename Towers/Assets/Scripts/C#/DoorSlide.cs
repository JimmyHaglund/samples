﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorSlide : MonoBehaviour
{
    bool isOpen = false;
    Vector3 startPos;
    void Start()
    {
        startPos = transform.position;
    }
    public void Activate()
    {
        if (isOpen || !gameObject.activeSelf)
            return;
        // Debug.Log("Opening door.");
        StartCoroutine(SlideDoor());
    }

    private IEnumerator SlideDoor()
    {
        yield return new WaitForFixedUpdate();
        if (transform.position.y - startPos.y <= 3.5f)
        {
            transform.position += new Vector3(0.0f, Time.fixedDeltaTime * 1.5f, 0.0f);
            StartCoroutine(SlideDoor());
        }
        yield return null;
    }
}

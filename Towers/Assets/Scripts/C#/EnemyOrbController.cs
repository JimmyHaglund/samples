﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PhysicalProjectile))]
public class EnemyOrbController : MonoBehaviour
{
    [SerializeField] private GameObject onDestroyEffect;
    [SerializeField] private Transform playerTransform;
    private Vector3 direction;
    private PhysicalProjectile projectileScript;

    private void Start()
    {
        projectileScript = GetComponent<PhysicalProjectile>();
        MoveTowardsPlayer();
    }

    public void MoveTowardsPlayer()
    {
        direction = playerTransform.position - transform.position;
        direction.Normalize();
        projectileScript.ProjectileDirection = direction;
    }

    public void SetPlayerTransform(Transform targetTransform)
    {
        playerTransform = targetTransform;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<PlayerHealth>())
        {
            collision.gameObject.GetComponent<PlayerHealth>().TakeDamage(50);
        }
        {
            GameObject newDestroyEffect = GameObject.Instantiate(onDestroyEffect);
            newDestroyEffect.transform.position = transform.position;
            GameObject.Destroy(gameObject);
        }
    }
}

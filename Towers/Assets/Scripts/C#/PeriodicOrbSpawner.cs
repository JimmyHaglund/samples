﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeriodicOrbSpawner : MonoBehaviour
{
    [SerializeField] private float spawnInterval = 2.0f;
    [SerializeField] private GameObject spawnedObject;
    [SerializeField] private Transform playerTransform;

    private void OnEnable()
    {
        StartCoroutine(Spawn());
    }

    IEnumerator Spawn()
    {
        while (true)
        {
            yield return new WaitForSeconds(spawnInterval);
            GameObject newObject = GameObject.Instantiate(spawnedObject);
            newObject.transform.position = transform.position;
            if (newObject.GetComponent<EnemyOrbController>() != null)
                newObject.GetComponent<EnemyOrbController>().SetPlayerTransform(playerTransform);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
/// <summary>
/// Class that runs an event when triggered by a collision.
/// Is destroyed on activation.
/// </summary>
public class Pickup : MonoBehaviour
{
    [SerializeField] private UnityEvent pickupEvent;
    [SerializeField] private GameObject parent;
    [SerializeField] private GameObject pickupEffect;

    private void OnTriggerEnter(Collider other)
    {
        pickupEvent.Invoke();
        if (pickupEffect != null)
        {
            GameObject effect = GameObject.Instantiate(pickupEffect);
            effect.transform.position = parent.transform.position;
        }
        GameObject.Destroy(parent);
    }
}

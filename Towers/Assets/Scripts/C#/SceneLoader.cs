﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] private  int targetScene;

    public void LoadScene()
    {
        SceneManager.LoadScene(targetScene);
        Time.timeScale = 1;
    }
    
}

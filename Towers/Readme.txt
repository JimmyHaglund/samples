Game playable at https://jimmyhaglund.itch.io/towers

Any assets not made by me (textures, fonts and particle sprites) were removed in this sample.
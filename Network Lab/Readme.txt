A lab assignment in a networking class I took during my time at Southern New Hampshire University.

The object of the lab was to create a client for a pre-determined server. The client had to send and accept the server's allowed commands or be punished.
The client was for a tactical grid-based game where players were split into two teams.

I said screw it and made my own team of unstoppable goblin bots. It was a good time.
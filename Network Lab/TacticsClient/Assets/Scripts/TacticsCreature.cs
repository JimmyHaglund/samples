﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///  Base class for any movable entity on the tactics grid.
/// </summary>
public class TacticsCreature : MonoBehaviour
{
    // Define character classes
    public enum Class
    {
        warrior = 1,
        rogue = 2,
        wizard = 3
    }
    public abstract class Archetype
    {
        public int classIndex = 0;
        public int Moverange = 3;
        public int Attackrange = 1;
        public int MaxHealth = 100;
        public int AttackDamage = 70;
    }
    public class Warrior : Archetype
    {
        public Warrior()
        {
            classIndex = 1;
            Moverange = 2;
            Attackrange = 1;
            MaxHealth = 100;
            AttackDamage = 70;
        }
    }
    public class Rogue : Archetype
    {
        public Rogue()
        {
            classIndex = 2;
            Moverange = 5;
            Attackrange = 1;
            MaxHealth = 70;
            AttackDamage = 30;
        }
    }
    public class Wizard : Archetype
    {
        public Wizard()
        {
            classIndex = 3;
            Moverange = 4;
            Attackrange = 6;
            MaxHealth = 30;
            AttackDamage = 30;
        }
    }
    private Archetype _currentClass;

    // Declare members
    private TacticsBoard.BoardPosition _position = new TacticsBoard.BoardPosition(0, 0);
    private int _health;
    private bool _isMoving;

    public Archetype GetClass()
    {
        return _currentClass;
    }
    public TacticsBoard.BoardPosition GetPosition()
    {
        return _position;
    }

    public void SetClass(int newClass)
    {
        switch (newClass)
        {
            case 1:
                _currentClass = new Warrior();
                break;
            case 2:
                _currentClass = new Rogue();
                break;
            case 3:
            default:
                _currentClass = new Wizard();
                break;
        }
    }
    public void SetClass(Class newClass)
    {
        SetClass((int)newClass);
    }
    public void SetHealth(int newHealth)
    {
        _health = newHealth;
        if (_health <= 0)
        {
            // Debug.Log("Creature " + gameObject.name + " is dead.");
            Renderer myRenderer = GetComponentInChildren<Renderer>();
            if (myRenderer != null) myRenderer.enabled = false;

        }
    }
    // Use this for initialization
    void Awake()
    {
        
    }

    public void Teleport(int positionX, int positionY)
    {
        // Prevent moving off the board
        if (positionX < 0) positionX = 0;
        if (positionX >= TacticsBoard.SizeX) positionX = TacticsBoard.SizeX - 1;
        if (positionY < 0) positionY = 0;
        if (positionY >= TacticsBoard.SizeY) positionY = TacticsBoard.SizeY - 1;

        TacticsBoard.SetSpace(_position.X, _position.Y, null);
        TacticsBoard.SetSpace(positionX, positionY, gameObject);
        transform.position = new Vector3(positionX, 0.0f, positionY);
        _position.X = positionX;
        _position.Y = positionY;
    }
    public void Teleport(TacticsBoard.BoardPosition position)
    {
        Teleport(position.X, position.Y);
    }
    public void Move(int  targetX, int targetY, float timeBetweenMoves = 0.25f)
    {
        if (_isMoving) return;
        Stack<TacticsBoard.BoardPosition> path = TacticsBoard.GetPath(_position.X, _position.Y, targetX, targetY);
        if (path == null) return;
        Move(path, timeBetweenMoves);
    }
    public void Move(Stack<TacticsBoard.BoardPosition> path, float timeBetweenMoves = 0.25f)
    {
        if (_isMoving || path == null) return;
        TacticsBoard.SetSpace(_position.X, _position.Y, null);
        StartCoroutine(TreadPath(path, timeBetweenMoves));
    }

    /// <summary>
    /// Checks if a target position is in range for movement. Returns a path that can be used if so.
    /// </summary>
    /// <param name="targetX"></param>
    /// <param name="targetY"></param>
    /// <returns></returns>
    public Stack<TacticsBoard.BoardPosition> CheckPath(int targetX, int targetY)
    {
        Stack<TacticsBoard.BoardPosition> path = null;
        // Check if target space is occupied or out of range
        if (TacticsBoard.GetSpace(targetX, targetY) != null) return null;
        if (TacticsBoard.OutOfRange(targetX, targetY)) return null;

        // Run path finding algorithm & compare required amount of steps to possible move range.
        path = TacticsBoard.GetPath(_position.X, _position.Y, targetX, targetY);
        if (path == null || path.Count > _currentClass.Moverange) return null;
        return path;
    }

    private IEnumerator TreadPath(Stack<TacticsBoard.BoardPosition> path, float timeBetweenSteps)
    {
        _isMoving = true;
        int remainingSteps = _currentClass.Moverange;
        while (remainingSteps-- > 0 && path.Count > 0)
        {
            TacticsBoard.BoardPosition nextStep = path.Pop();
            transform.position = new Vector3(nextStep.X, 0.0f, nextStep.Y);
            _position = nextStep;
            yield return new WaitForSeconds(timeBetweenSteps);
        }
        _isMoving = false;
        TacticsBoard.SetSpace(_position.X, _position.Y, gameObject);
    }

    private int PositionDistance(TacticsBoard.BoardPosition posA, TacticsBoard.BoardPosition posB)
    {
        int distanceX = Mathf.Abs(posA.X - posB.X);
        int distanceY = Mathf.Abs(posA.Y - posB.Y);

        return (distanceX + distanceY);
    }
    public void Attack(int targetX, int targetY)
    {

    }

    public bool Moving{ get { return _isMoving; } }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static Dictionary<int, TacticsInput> Players = new Dictionary<int, TacticsInput>(50);
    public static List<int> PlayerIds = new List<int>(50);
    private static GameManager _instance;
    private static Dictionary<int, int> Teams = new Dictionary<int, int>(50);
    public int PlayerTurnID = 0;
    [HideInInspector] public Event_IntStr PlayerNameChanged;
    [HideInInspector] public Event_Str MessageReceived;
    [SerializeField] private GameObject _playerObjectPrefab = null;
    // [SerializeField] private GameObject _networkCreaturePrefab = null;
    private List<int> _localPlayers = new List<int>(1);

    [SerializeField]private bool _networkEnabled = false;

    public enum GameState
    {
        menu,
        game
    }
    public GameState State = GameState.menu;
    
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("No Game Manager found in scene!");
            }
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null) Destroy(gameObject);
        else _instance = this;
    }
    private void Start()
    {
        if (GameObject.FindObjectOfType<TacticsClient>() != null) _networkEnabled = true;
    }

    public void ChangePlayerName (int playerId, string newName)
    {
        if (!Players.ContainsKey(playerId)) return;
        if (Players[playerId].Client == null)
        {
            GameObject playerObject = Players[playerId].gameObject;
            playerObject.name = newName;
            PlayerNameChanged.Invoke(playerId, newName);

        }
    }

    public void SetTeam(int playerId, int teamNumber)
    {
        Teams.Add(playerId, teamNumber);
    }

    public void AddPlayer(int playerId, int teamNumber, TacticsClient client = null)
    {
        // Create new player object.
        if (!Players.ContainsKey(playerId))
        {
            GameObject newObject = GameObject.Instantiate(_playerObjectPrefab);
            TacticsInput newController = newObject.GetComponent<TacticsInput>();
            if (newController == null)
            {
                newController = newObject.AddComponent<TacticsInput>();
            }
            if (newObject.GetComponent<TacticsCreature>() == null)
            {
                newObject.AddComponent<TacticsCreature>();
            }
            Players.Add(playerId, newController);
            Teams.Add(playerId, teamNumber);

            Players[playerId].Id = playerId;
            Players[playerId].gameObject.SetActive(true);
            PlayerIds.Add(playerId);
        }

        // If client argument is defined, new player will be controlled locally.
        if (_networkEnabled && client != null)
        {
            Players[playerId].SetUpNetwork(client);
        }
    }

    public void ChatMessage(string message)
    {
        MessageReceived.Invoke(message);
    }

    public void PlayerReady(int playerId, bool readyState)
    {
        // Debug.Log("Player " + playerId + (readyState ? " is ready." : "is NOT PREPARED."));
    }

    public void StartGame(int inSeconds)
    {
        Debug.Log("Game set to start in " + inSeconds + " seconds.");
    }
}

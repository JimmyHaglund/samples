﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantRotateAroundPoint : MonoBehaviour
{
    [SerializeField] private float _rotationSpeed = 45.0f;

    private Vector3 _rotationPivot;
    public void SetRotation()
    {
        _rotationPivot = TacticsBoard.GetBoardCenterPoint();
        transform.position = _rotationPivot - transform.forward * 15.0f;
    }

    private void Update()
    {
        transform.RotateAround(_rotationPivot, Vector3.up, _rotationSpeed * Time.deltaTime);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TacticsGoblinAI : TacticsInput
{
    private static List<TacticsGoblinAI> _GoblinArmy = new List<TacticsGoblinAI>(20);
    private static TacticsGoblinAI _GoblinKing = null;

    [SerializeField] private GameObject _modelRoot = null;
    [SerializeField] private GameObject _altModel = null;

    private TacticsBoard.BoardPosition _targetPosition;
    private Stack<TacticsBoard.BoardPosition> _movementPath;
    private int _remainingMoves = 0;
    

    [SerializeField] private float _moveSpeed = 0.05f;
    private enum Formation
    {
        chaos,
        wedge,
        spreadOut,
        circle
    };
    private enum AgressionState
    {
        runAway,
        attackInRange,
        formationFirst,
        archerKite
    }

    private void Awake()
    {
        Equip();
        // Ready(true);
        
    }
    private string ClassName(TacticsCreature.Class type)
    {
        switch (type)
        {
            case TacticsCreature.Class.warrior:
                return "Swashbuckler";
            case TacticsCreature.Class.rogue:
                return "Backstabber";
            case TacticsCreature.Class.wizard:
            default:
                return "Archer";
        }
    }
    private bool _myturn = false;
    #region Menu phase methods
    
    protected override void Ready(bool readyState)
    {
        // Debug.Log("Goblin " + Id + " readying up!");
        base.Ready(readyState);
    }
    public override void ChangeName(string newName)
    {
        gameObject.name = "AI " + newName;
        if (Client != null)
        {
            Client.gameObject.name = newName;
        }
        base.ChangeName(newName);
    }
    private void NameChangeConfirmed(string name)
    {
        // Ready(true);
    }
    protected override void ChangeClass(int classType)
    {
        base.ChangeClass(classType);
    }
    private void ClassChangeConfirmed(int classType)
    {
        ChangeName("Goblin " + ClassName((TacticsCreature.Class)classType) + " (" + Id + ")");
    }
    protected override void ChangeClass(TacticsCreature.Class classType)
    {
        ChangeClass((int)classType);
    }

    public override void SetUpNetwork(TacticsClient client)
    {
        base.SetUpNetwork(client);
        if (Client != null)
        {
            // Set up events & start talking to server about setting self to be ready.
            Client.CreatureMoved.AddListener(TakeStep);
            Client.NameChanged.AddListener(NameChangeConfirmed);
            Client.ClassChanged.AddListener(ClassChangeConfirmed);
            Client.TurnStarted.AddListener(StartTurn);
            Client.MessageReceived.AddListener(InterpretMessage);
            Client.HealthChanged.AddListener(ForceTurnEnd);
            BootCamp();

            // Change to model representing locally controlled creature
            Vector3 pos = _modelRoot.transform.position;
            GameObject newBody = GameObject.Instantiate(_altModel);
            newBody.transform.position = pos;
            newBody.transform.parent = transform;
            GameObject.Destroy(_modelRoot);
        }
    }
    private void BootCamp()
    {
        ChangeClass(Random.Range(1, 3));
    }
    private void Equip()
    {
        /*
        if (_GoblinKing == null)
        {
            _GoblinKing = this;
            ChangeClass(TacticsCreature.Class.warrior);
            ChangeName("Goblin King");
        }
        else
        */
        {
            // _GoblinArmy.Add(this);
            // ChangeClass(Random.Range(1, 3));
            // ChangeName("Goblin " + ClassName((TacticsCreature.Class)_controlledCreature.GetClass().classIndex) + Id);
        }
    }
    #endregion

    #region Game Phase methods

    public override void StartTurn()
    {
        // Debug.Log("Starting turn for " + gameObject.name);

        if (Client != null)
        {
            _myturn = true;
            _remainingMoves = _controlledCreature.GetClass().Moverange;
            /*
            if (_GoblinKing == this)
            {
                PassTurn();
            }
            else
            {
                _targetPosition = _GoblinKing.Creature.GetPosition();
                // _controlledCreature.Move(_targetPosition.X, _targetPosition.Y, 0.05f);
            }
            */
            if (FindEnemyInRange()) return;
            MoveRandom(); // TODO: Replace with actual AI
            // StartCoroutine(TurnTimer());
        }
    }
    
    /*
    public IEnumerator TurnTimer()
    {
        yield return new WaitForSeconds(1.0f);
        if (_myturn)
        {
            EndTurn();
        }
    }
    */

    protected override void EndTurn(bool sendMessage = false)
    {
        // Debug.LogWarning("Manually ending turn for " + gameObject.name + ". Notify server: " + sendMessage);
        _myturn = false;
        base.EndTurn(sendMessage);
    }
    protected override void PassTurn()
    {
        base.PassTurn();
    }
    protected override void Attack(int targetX, int targetY)
    {
        base.Attack(targetX, targetY);
    }

    protected override void PrintMessage()
    {
        base.PrintMessage();
    }
    protected override void PrintTeamMessage(string message)
    {
        base.PrintTeamMessage(message);
    }

    /// <summary>
    /// Forces turn to end & notify server - useful to make sure game 
    /// keeps going even if AI is performing erronous commands.
    /// </summary>
    private void ForceTurnEnd()
    {
        if (Client != null && _myturn)
            EndTurn(true);
    }
    private void InterpretMessage(string message)
    {
        if (!message.Contains("Goblin"))
        {
            Debug.LogWarning("Not contain golibin");
            return;
        }
        if (message.Contains("Ready"))
        {
            Ready(true);
        }
        else
        {
            Debug.LogWarning("Not vcoantin Ready");
        }
    }
    private void SetTactics(Formation targetFormation)
    {

    }
    private void MoveRandom()
    {
        TacticsBoard.BoardPosition position = RandomPositionInRange();
        Stack<TacticsBoard.BoardPosition> path = _controlledCreature.CheckPath(position.X, position.Y);
        int count = 0;
        while (count++ < 100 && (TacticsBoard.GetSpace(position.X, position.Y) != null || path == null))
        {
            position = RandomPositionInRange();
            path = _controlledCreature.CheckPath(position.X, position.Y);
        }
        if (count >= 99)
        {
            // Debug.Log("Tried to get a position too many times.");
            EndTurn();
        }
        // Debug.Log("Found target position at " + position.X + ", " + position.Y);
        if (path == null)
        {
            // Debug.Log("Path is null - can't move!.");
            EndTurn(true);
            return;
        }
        _movementPath = path;
        // _remainingMoves = path.Count;
        TakeStep(Id);
    }
    private TacticsBoard.BoardPosition RandomPositionInRange()
    {
        int moveRange = _controlledCreature.GetClass().Moverange;
        int moveX = Random.Range(0, moveRange);
        int moveY = moveRange - moveX;
        if (Random.value > 0.5f) moveX *= -1;
        if (Random.value > 0.5f) moveY *= -1;

        TacticsBoard.BoardPosition move = new TacticsBoard.BoardPosition(moveX, moveY);
        move += _controlledCreature.GetPosition();
        move.X = (int)Mathf.Clamp(move.X, 0, TacticsBoard.SizeX - 1);
        move.Y = (int)Mathf.Clamp(move.Y, 0, TacticsBoard.SizeY - 1);
        return (move);
    }
    private void TakeStep(int id)
    {
        if (!_myturn)
        {
            // Debug.Log("Id " + Client.OwnedID + " can't move: not my turn.");
            return;

        }
        if (Client != null)
        {
            if (_movementPath == null || (_movementPath.Count == 0 && _remainingMoves > 0))
            {
                EndTurn(true);
                return;
            }
            TacticsBoard.BoardPosition position = _movementPath.Pop();
            /*
            Debug.Log("Moving creature" + name + " from position "
                + _controlledCreature.GetPosition().X
                + ", "
                + _controlledCreature.GetPosition().Y
                + " to "
                + position.X
                + ", "
                + position.Y);
            */
            _controlledCreature.Teleport(position);
            Client.OnMove(position.X, position.Y);
            if (--_remainingMoves <= 0)
            {
                _movementPath = null;
                EndTurn(false);
            }
        }
        else
        {
            // Debug.Log("Takestep was called but not for this player.");
        }
    }
    private bool FindEnemyInRange()
    {
        for (int n = 0; n < GameManager.Players.Count; ++n)
        {
            TacticsInput player = GameManager.Players[GameManager.PlayerIds[n]];
            TacticsBoard.BoardPosition myPos = Creature.GetPosition();
            TacticsBoard.BoardPosition theirPos = player.Creature.GetPosition();
            if (TacticsBoard.Distance(myPos.X, myPos.Y, theirPos.X, theirPos.Y) == 1);
            {
                if (!player.gameObject.name.Contains("Goblin"))
                {
                    /*
                    Debug.Log("Creature at position "
                    + _controlledCreature.GetPosition().X
                    + ", "
                    + _controlledCreature.GetPosition().Y
                    + " attacking position "
                    + theirPos.X
                    + ", "
                    + theirPos.Y);
                    Attack(theirPos.X, theirPos.Y);
                    EndTurn(false);
                    return true;
                    */
                }
            }
        }
        return false;
    }
    
    #endregion
}
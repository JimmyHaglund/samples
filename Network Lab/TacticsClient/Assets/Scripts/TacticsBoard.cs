﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TacticsBoard
{
    public static bool DebugEnabled = false;
    // Wrapper class for on cleared event - 'kind of' allows static event
    public class OnClearedEvent
    {
        public UnityEngine.Events.UnityEvent BoardCleared = new UnityEngine.Events.UnityEvent();
        public void Invoke()
        {
            BoardCleared.Invoke();
        }
        public void AddListener(UnityEngine.Events.UnityAction call)
        {
            BoardCleared.AddListener(call);
        }
        public void RemoveAllListeners()
        {
            BoardCleared.RemoveAllListeners();
        }
    }
    public static OnClearedEvent BoardCleared = new OnClearedEvent();

    public static int SizeX = 0;
    public static int SizeY = 0;
    private static List<List<BoardSpace>> _boardSpaces;

    public static Vector3 GetBoardCenterPoint()
    {
        return new Vector3((float)SizeX / 2.0f, 0.0f, (float)SizeY / 2.0f);
    }
    public class BoardPosition
    {
        public int X = 0;
        public int Y = 0;
        public BoardPosition(int x, int y)
        {
            X = x;
            Y = y;
        }
        public static BoardPosition operator + (BoardPosition positionA, BoardPosition positionB)
        {
            BoardPosition newPosition = new BoardPosition
            (
                positionA.X + positionB.X,
                positionA.Y + positionB.Y
            );
            return newPosition;
        }
    }
    private class BoardSpace
    {
        public BoardSpace(int posX, int posY, GameObject groundSpace)
        {
            Position = new BoardPosition(posX, posY);
            groundSpace.transform.position = new Vector3(posX, 0.0f, posY);
            ContainerText = groundSpace.GetComponentInChildren<TextMesh>();
        }
        public BoardPosition Position;
        public GameObject ContainedObject = null;
        public GameObject GroundSpace = null;
        public TextMesh ContainerText = null;
    }
    public static GameObject GetSpace(int x, int y)
    {
        if (x < 0 || x >= _boardSpaces.Count) return null;
        if (y < 0 || y >= _boardSpaces[x].Count) return null;

        return _boardSpaces[x][y].ContainedObject;
    }
    public static GameObject GetSpace(BoardPosition position)
    {
        return GetSpace(position.X, position.Y);
    }
    public static bool OutOfRange(int x, int y)
    {
        if (x < 0 || x >= _boardSpaces.Count) return true;
        if (y < 0 || y >= _boardSpaces[x].Count) return true;
        return false;
    }
    public static void SetSpace(int x, int y, GameObject value)
    {
        if (x < 0 || x >= SizeX) return;
        if (y < 0 || y >= SizeY) return;
        // if (_boardSpaces[x][y] != null) return;

        _boardSpaces[x][y].ContainedObject = value;
        // Debug.Log("Setting space " + x + ", "+ y + " to " + value);
        if (_boardSpaces[x][y].ContainerText != null)
        {
            if (value == null)
            {
                _boardSpaces[x][y].ContainerText.text = "N/A";// _boardSpaces[x][y].ContainedObject.ToString();
            }
            else
            {
                _boardSpaces[x][y].ContainerText.text = _boardSpaces[x][y].ContainedObject.name;
            }
        }
    }
    public static void SetBoardSize(int sizeX, int sizeY)
    {
        Debug.Log("Board size set to " + sizeX + ", " + SizeY);
        SizeX = sizeX;
        SizeY = sizeY;
        if (_boardSpaces != null)
        {
            for (int q = 0; q < _boardSpaces.Count; ++q)
            {
                foreach (BoardSpace space in _boardSpaces[q])
                {
                    GameObject.Destroy(space.GroundSpace);
                }
            }
        }
        _boardSpaces = new List<List<BoardSpace>>(sizeX);
        for (int n = 0; n < sizeX; ++n)
        {
            _boardSpaces.Add(new List<BoardSpace>(SizeY));
            for (int i = 0; i < sizeY; ++i)
            {
                _boardSpaces[n].Add(new BoardSpace(n, i, GameObject.Instantiate((GameObject)Resources.Load("Environment/GroundSpace"))));
            }
        }
    }

    public static void SetSpaceBlocked(int positionX, int positionY)
    {
        _boardSpaces[positionX][positionY].ContainedObject = GameObject.Instantiate((GameObject)Resources.Load("Environment/Rock"));
        _boardSpaces[positionX][positionY].ContainedObject.transform.position = new Vector3(positionX, 0.0f, positionY);
        if (_boardSpaces[positionX][positionY].ContainerText != null)
        {
            _boardSpaces[positionX][positionY].ContainerText.text = _boardSpaces[positionX][positionY].ContainedObject.name;
        }
    }

    public static int Distance(int xA, int yA, int xB, int yB)
    {
        return (Mathf.Abs(xA - xB) + Mathf.Abs(yA - yB));
    }
    #region Path Finding

    #region Map exploration
    /// <summary>
    /// Retrieves nearest possible path between two points on the board.
    /// </summary>
    /// <param name="startX"></param>
    /// <param name="startY"></param>
    /// <param name="endX"></param>
    /// <param name="endY"></param>
    public static Stack<BoardPosition> GetPath(int startX, int startY, int endX, int endY)
    {
        BoardCleared.Invoke();
        BoardCleared.RemoveAllListeners();
        BoardPosition target = new BoardPosition(endX, endY);
        
        // Initialize nodes grid
        List<List<PathNode>> nodesGrid = new List<List<PathNode>>(TacticsBoard.SizeX);
        for (int n = 0; n < TacticsBoard.SizeX; n++)
        {
            nodesGrid.Add(new List<PathNode>(TacticsBoard.SizeY));
            for (int i = 0; i < TacticsBoard.SizeY; i++)
            {
                nodesGrid[n].Add(null);
            }
        }
        // Set start node
        PathNode startNode = NewNode(startX, startY, startX, startY, endX, endY, nodesGrid, 0);
        startNode.state = NodeState.explored;

        // Explore possible paths to target
        nodesGrid = ExplorePath(startNode, startNode.Position, target, nodesGrid);

        // Get best / least distance path to target
        Stack<BoardPosition> path = GetNearestPath(startNode.Position, target, nodesGrid);
        return path;
    }

    // Recursive function which explores a path from one position to another.
    private static List<List<PathNode>> ExplorePath(PathNode currentNode, BoardPosition startPosition, BoardPosition targetPosition, List<List<PathNode>> nodesGrid)
    {
        // Base case 2: no possible path found if currentNode = null.
        if (currentNode == null)
        {
            goto NULLNODE;
        }
        // Base case: Target position found
        if (Distance(currentNode.Position.X, currentNode.Position.Y, targetPosition.X, targetPosition.Y) == 0)
        {
            if (DebugEnabled)
            {
                GameObject marker = GameObject.Instantiate((GameObject)Resources.Load("SelectedMarker"));
                marker.transform.position = new Vector3(targetPosition.X, 0.0f, targetPosition.Y);
            }
            return nodesGrid;
        }
        // If current node is occupied, find least distance travelled unexplored node & set that as next target.
        if (currentNode.state == NodeState.blocked)
        {
            PathNode nextNode = FindNearestToStartUnexploredNode(nodesGrid);
            return ExplorePath(nextNode, startPosition, targetPosition, nodesGrid);
        }
        // Check surrounding unexplored nodes for nearest to target, set as next to be explored.
        else
        {
            currentNode.state = NodeState.explored;
            if (DebugEnabled)
            {
                GameObject marker = GameObject.Instantiate((GameObject)Resources.Load("ExploredMarker"));
                marker.transform.position = new Vector3(currentNode.Position.X, 0, currentNode.Position.Y);
            }
            List<PathNode> potentialNodes = AdjacentUnexploredNodes(currentNode, nodesGrid, startPosition, targetPosition);
            // If no unexplored surrounding nodes were found, find least travelled node.
            if (potentialNodes.Count == 0)
            {
                PathNode nextNode = FindNearestToStartUnexploredNode(nodesGrid);
                return ExplorePath(nextNode, startPosition, targetPosition, nodesGrid);
            }
            // Set nearest surrounding node as next node.
            else
            {
                PathNode nextNode = ClosestNodeToTarget(potentialNodes);
                return ExplorePath(nextNode, startPosition, targetPosition, nodesGrid);
            }
        }
    NULLNODE:
        return null;
    }

    // Find unexplored node surrounding a position.
    private static List<PathNode> AdjacentUnexploredNodes(PathNode adjacentNode, List<List<PathNode>> nodesGrid, BoardPosition startPosition, BoardPosition targetPosition)
    {
        List<PathNode> nodes = new List<PathNode>(4);
        BoardPosition position = adjacentNode.Position;

        List<BoardPosition> positions = new List<BoardPosition>(4);
        positions.Add(new BoardPosition(position.X + 1, position.Y));
        positions.Add(new BoardPosition(position.X - 1, position.Y));
        positions.Add(new BoardPosition(position.X, position.Y + 1));
        positions.Add(new BoardPosition(position.X, position.Y - 1));

        for (int n = 0; n < 4; n++)
        {
            if (OutOfRange(positions[n].X, positions[n].Y)) continue;
            if (nodesGrid[positions[n].X][positions[n].Y] == null)
            {
                nodes.Add(NewNode(positions[n].X, positions[n].Y, startPosition, targetPosition, nodesGrid, adjacentNode.DistanceTravelled + 1));
            }
            else if (nodesGrid[positions[n].X][positions[n].Y].state == NodeState.unexplored)
            {
                nodes.Add(nodesGrid[positions[n].X][positions[n].Y]);
            }
        }

        return nodes;
    }

    // Creates new Path node & sets up values correctly.
    private static PathNode NewNode(int positionX, int positionY, BoardPosition startPosition, BoardPosition targetPosition, List<List<PathNode>> nodesGrid, int distanceTravelled)
    {
        // Debug.Log("Creating new node at " + positionX + ", " + positionY);
        PathNode node = new PathNode();
        node.Position = new BoardPosition(positionX, positionY);
        // Check if node is blocked or something
        if (TacticsBoard.GetSpace(positionX, positionY) != null
        || TacticsBoard.OutOfRange(positionX, positionY))
        {
            node.state = NodeState.blocked;
        }
        else if (DebugEnabled)
        {
            GameObject marker = GameObject.Instantiate((GameObject)Resources.Load("OpenMarker"));
            marker.transform.position = new Vector3(node.Position.X, 0, node.Position.Y);
        }
        node.DistanceToTarget = Distance(positionX, positionY, targetPosition.X, targetPosition.Y);
        node.DistanceToStart = Distance(positionX, positionY, startPosition.X, startPosition.Y);
        node.DistanceTravelled = distanceTravelled;
        nodesGrid[positionX][positionY] = node;
        return node;
    }
    private static PathNode NewNode(int positionX, int positionY, int startX, int startY, int targetX, int targetY, List<List<PathNode>> nodesGrid, int distanceTravelled)
    {
        BoardPosition startPosition = new BoardPosition(startX, startY);
        BoardPosition targetPosition = new BoardPosition(targetX, targetY);
        return NewNode(positionX, positionY, startPosition, targetPosition, nodesGrid, distanceTravelled);
    }

    // Finds least travelled node in a grid of nodes.
    private static PathNode FindNearestToStartUnexploredNode(List<List<PathNode>> nodesGrid)
    {
        // Debug.Log("Finding least travelled unexplored node.");
        PathNode leastTravelled = null;
        int leastDistance = 10000;
        for (int n = 0; n < nodesGrid.Count; n++)
        {
            for (int i = 0; i < nodesGrid[n].Count; i++)
            {
                // Check that node exists & is unexplored
                if (nodesGrid[n][i] == null) continue;
                if (nodesGrid[n][i].state != NodeState.unexplored) continue;
                // Check distance travelled.
                if (nodesGrid[n][i].DistanceToStart < leastDistance)
                {
                    leastDistance = nodesGrid[n][i].DistanceToStart;
                    leastTravelled = nodesGrid[n][i];
                }
            }
        }
        return leastTravelled;
    }
    #endregion

    #region Finding nearest path
    // Retrieves closest node to target from a list of nodes
    private static PathNode ClosestNodeToTarget(List<PathNode> nodes)
    {
        PathNode closest = null;
        int closestDistance = 10000;
        for (int n = 0; n < nodes.Count; n++)
        {
            if (nodes[n].DistanceToTarget < closestDistance)
            {
                closest = nodes[n];
                closestDistance = nodes[n].DistanceToTarget;
            }
        }
        return closest;
    }

    // Locates closest path among pathnodes 
    private static Stack<BoardPosition> GetNearestPath(BoardPosition startPosition, BoardPosition endPosition, List<List<PathNode>> nodesGrid)
    {
        if (nodesGrid == null) return null;
        // Initialize path stack
        Stack<BoardPosition> path = new Stack<BoardPosition>(TacticsBoard.SizeX * 2);
        path.Push(endPosition);
        // Loop through from the end of the path to find the best path to target.
        int counter = 0;
        while ((path.Peek().X != startPosition.X || path.Peek().Y != startPosition.Y && counter++ < 300))
        {
            List<PathNode> adjacentNodes = AdjacentExploredNodes(path.Peek(), nodesGrid);
            path.Push(LeastTravelledNode(adjacentNodes));

            // Debug - create inactive path nodes.
            if (DebugEnabled)
            {
                GameObject marker = GameObject.Instantiate((GameObject)Resources.Load("SelectedMarker"));
                marker.transform.position = new Vector3(path.Peek().X, 0.0f, path.Peek().Y);
            }
        }
        if (counter >= 299)
            Debug.Log("Counter at " + counter + ". Stopping search!");
        // Remove starting position - it should be where the creature is currently standing, not where it should travel.
        path.Pop();
        return path;
    }

    private static List<PathNode> AdjacentExploredNodes(BoardPosition position, List<List<PathNode>> nodes)
    {
        List<BoardPosition> positions = new List<BoardPosition>(4);
        positions.Add(new BoardPosition(position.X + 1, position.Y));
        positions.Add(new BoardPosition(position.X - 1, position.Y));
        positions.Add(new BoardPosition(position.X, position.Y + 1));
        positions.Add(new BoardPosition(position.X, position.Y - 1));
        List<PathNode> foundNodes = new List<PathNode>(4);

        for (int n = 0; n < positions.Count; n++)
        {
            if (TacticsBoard.OutOfRange(positions[n].X, positions[n].Y)) continue;

            if (nodes[positions[n].X][positions[n].Y] == null) continue;

            if (nodes[positions[n].X][positions[n].Y].state == NodeState.explored)
            {
                foundNodes.Add(nodes[positions[n].X][positions[n].Y]);
            }
        }
        return foundNodes;
    }
    private static BoardPosition LeastTravelledNode(List<PathNode> nodes)
    {
        PathNode leastTravelledNode = null;
        int leastTravelledDistance = 10000;
        for (int n = 0; n < nodes.Count; ++n)
        {
            if (nodes[n].DistanceTravelled < leastTravelledDistance)
            {
                leastTravelledNode = nodes[n];
                leastTravelledDistance = nodes[n].DistanceTravelled;
            }
        }
        return leastTravelledNode.Position;
    }

    private enum NodeState
    {
        unexplored,
        explored,
        blocked
    }

    private class PathNode
    {
        public BoardPosition Position;
        public int DistanceToTarget = 10000;
        public int DistanceToStart = 10000;
        public int DistanceTravelled = 10000;
        public NodeState state = NodeState.unexplored;
    }
    #endregion
    #endregion
}
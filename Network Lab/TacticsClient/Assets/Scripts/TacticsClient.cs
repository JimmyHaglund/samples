﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TacticsClient : MonoBehaviour
{
    // Static reference to whichever client should handle all incoming messages for non-owned creatures.
    private static TacticsClient _ogClient = null;

    [SerializeField] private int _portNumber = 603;
    [SerializeField] private UnityEngine.UI.Text _ipText;

    [SerializeField] private UnityEngine.UI.Text _messageText;
    [SerializeField] private UnityEngine.UI.Button _messageButton;
    [SerializeField] private UnityEngine.UI.Button _connectButton; 

    // [SerializeField] private string _PlayerObjectPrefabName = "Creature";
    // [SerializeField] private TacticsInput _inputScript;
    private GameObject _localObject;

    public ClientNetwork ClientNetwork;
    [HideInInspector] public Event_Int CreatureMoved;
    [HideInInspector] public Event_Str NameChanged;
    [HideInInspector] public Event_Int ClassChanged;
    [HideInInspector] public UnityEngine.Events.UnityEvent TurnStarted;
    [HideInInspector] public Event_Str MessageReceived;
    [HideInInspector] public UnityEngine.Events.UnityEvent HealthChanged;

    private int _clientId = -1;
    private int _teamNumber = -1;
    private bool _loginInProcess = false;

    public int OwnedID
    {
        get
        {
            return _clientId;
        }
    }
    public int OwnedTeam
    {
        get
        {
            return _teamNumber;
        }
    }

    private void Start()
    {
        if (_connectButton != null)
        {
            _connectButton.onClick.AddListener(ConnectToServer);
        }
    }

    public void ConnectToServer()
    {
        string adress = "127.0.0.1";
        if (_ipText != null && _ipText.text != "")
        {
            adress = _ipText.text;
        }

        ConnectToServer(adress, _portNumber);
    }
    public void ConnectToServer(string serverAdress, int portNumber)
    {
        // Debug.Log("Attempting to connect to server...");
        if (_loginInProcess) return;
        _loginInProcess = true;
        ClientNetwork.Connect(serverAdress, portNumber, "", "", "", 0);
    }

    #region Recieved RPC calls Server -> Client [Login Phase]
    /// <summary>
    /// "Called to tell your client what your played id will be for the session"
    /// </summary>
    /// <param name="playerId"></param>
    public void SetPlayerId(int playerId)
    {
        _clientId = playerId;
    }

    /// <summary>
    /// "Team will be 1 or 2"
    /// </summary>
    /// <param name="team"></param>
    public void SetTeam(int team)
    {
        // Debug.Log("Receiving set team call on id " + _clientId);
        GameManager.Instance.AddPlayer(_clientId, team, this);
        _teamNumber = team;
        // Set client that will handle all non-owned objects.
        if (_ogClient == null)
        {
            if (_messageButton != null)
            {
                _messageButton.onClick.AddListener(OnMessage);
            }
            else
            {
                Debug.LogWarning("Message button is null.");
            }
            _ogClient = this;
        }
    }

    /// <summary>
    /// "Another player has connected to the game"
    /// </summary>
    /// <param name="playerId"></param>
    /// <param name="team"></param>
    public void NewPlayerConnected(int playerId, int team)
    {
        if (_ogClient != this) return;
        // Debug.Log("New player connected called: " +  playerId + ", " + team);
        if (playerId == 0)
        {
            //Debug.LogWarning("Received connection call for client that has not been assigned an Id yet.");
            return;
        }
        if (GameManager.Players.ContainsKey(playerId)) return;
        GameManager.Instance.AddPlayer(playerId, team);
    }

    /// <summary>
    /// "Another player has changed their name"
    /// </summary>
    /// <param name="playerId"></param>
    /// <param name="name"></param>
    public void PlayerNameChanged(int playerId, string name)
    {
        if (playerId != _clientId && _ogClient != this) return;

        NameChanged.Invoke(name);

        if (_ogClient == this)
        {
            GameManager.Instance.ChangePlayerName(playerId, name);
        }
    }

    /// <summary>
    /// "Another player is ready to play"
    /// </summary>
    /// <param name="playerId"></param>
    /// <param name="isReady"></param>
    public void PlayerIsReady(int playerId, bool isReady)
    {
        if (playerId != _clientId && _ogClient != this) return;
        // Debug.Log("Player is ready called.");
        // PlayerReady.Invoke(playerId, isReady);
        if (_ogClient == this)
        {
            GameManager.Instance.PlayerReady(playerId, isReady);
        }
    }

    /// <summary>
    /// "Another player has changed their character type"
    /// </summary>
    /// <param name="playerId"></param>
    /// <param name="type"></param>
    public void PlayerClassChanged(int playerId, int type)
    {
        if (playerId != _clientId && _ogClient != this) return;
        // Debug.Log("Player class changed called.");
        // _activePlayers[playerId].SetClass(type);
        ClassChanged.Invoke(type);
        if (_ogClient == this)
        {
            GameManager.Players[playerId].Creature.SetClass(type);

        }
    }

    /// <summary>
    /// "The game will start after the given amount of time"
    /// </summary>
    /// <param name="time"></param>
    public void GameStart(int time)
    {
        if (_ogClient != this) return;
        GameManager.Instance.StartGame(time);
    }
    #endregion

    #region Sent RPC calls Client -> Server [Login Phase]
    public void OnNameChange(string newName)
    {
        ClientNetwork.CallRPC("SetName", UCNetwork.MessageReceiver.ServerOnly, -1, newName); // Set the name of your character
    }

    public void OnClassChange(int newClass)
    {
        // Debug.Log("Class change : " + newClass);
        ClientNetwork.CallRPC("SetCharacterType", UCNetwork.MessageReceiver.ServerOnly, - 1, newClass); // Set the type of character you would like to be (See Id's in instructions.txt)
    }

    public void OnReady(bool readyState)
    {
        // Debug.Log("On Ready called!");
        ClientNetwork.CallRPC("Ready", UCNetwork.MessageReceiver.ServerOnly, -1, readyState); // Call when your client is ready to play, or is no longer ready to play
    }
    #endregion


    #region Recieved RPC calls Server -> Client [Game Phase]
    // "Tell the client the size of the map."
    public void SetMapSize(int sizeX, int sizeY)
    {
        if (_ogClient != this) return;
        Debug.Log("Generating board space");
        TacticsBoard.SetBoardSize(sizeX, sizeY);
    }

    // "Tell the client that a specific space on the map is 'blocked'."
    public void SetBlockedSpace(int positionX, int positionY)
    {
        if (_ogClient != this) return;
        TacticsBoard.SetSpaceBlocked(positionX, positionY);
        // BlockedSpaceSet.Invoke(sizeX, sizeY);
    }

    // "A player has moved and is now at the given position."
    public void SetPlayerPosition(int playerId, int positionX, int positionY)
    {
        if (playerId != _clientId && _ogClient != this) return;

        if (_clientId == playerId)
        {
            CreatureMoved.Invoke(playerId);
        }


        if (_ogClient == this)
        {
            GameManager.Players[playerId].Creature.Teleport(positionX, positionY);

        }
    }

    // "Called when it is the start of the given player's turn."
    public void StartTurn(int playerId)
    {
        if (playerId == _clientId)
        {
            TurnStarted.Invoke();
        }
        if (_ogClient == this)
        {
            GameManager.Instance.PlayerTurnID = playerId;
            // GameManager.Instance.BeginTurn(playerID);
        }
        // TurnStarted.Invoke(playerID);
    }

    // "The given player just made an attack at the given location."
    public void AttackMade(int playerId, int positionX, int positionY)
    {
        // _activePlayers[playerId].Attack(positionX, positionY);
        // AttackWasMade.Invoke(playerId, positionX, positionY);
        GameManager.Players[playerId].Creature.Attack(positionX, positionY);
    }

    // "Display a chat message from another client or the server."
    public void DisplayChatMessage(string message)
    {
        Debug.Log(message);
        MessageReceived.Invoke(message);
    }

    // "Update the health of a player, will be called whenever a player's health changes."
    public void UpdateHealth(int playerId, int newHealth)
    {
        // _activePlayers[playerId].SetHealth(newHealth);
        // HealthUpdated.Invoke(playerId, newHealth);
        GameManager.Players[playerId].Creature.SetHealth(newHealth);
        HealthChanged.Invoke();
    }
    #endregion
    
    #region Sent RPC calls Client -> Server [Game Phase]
    public void OnMove(int positionX, int positionY)
    {
        // if (!_myTurn) return;
        // _myTurn = false;

        // Debug.Log("Sending move request from: " + _clientId);
        ClientNetwork.CallRPC("RequestMove", UCNetwork.MessageReceiver.ServerOnly, -1, positionX, positionY);
    }

    public void OnAttack(int targetX, int targetY)
    {
        Debug.Log("Sending attack request.");
        ClientNetwork.CallRPC("RequestAttack", UCNetwork.MessageReceiver.ServerOnly, -1, targetX, targetY);
    }

    public void OnMessage()
    {
        Debug.Log("OnMessage called.");
        if (_ogClient != this) return;
        string message = "";
        if (_messageText != null)
        {
            message = _messageText.text;
        }
        else
        {
            Debug.LogWarning("Message text object is null.");
        }
        if (message == "") return;
        ClientNetwork.CallRPC("SendChat", UCNetwork.MessageReceiver.ServerOnly, -1, message);
    }

    public void OnTeamMessage(string message)
    {
        ClientNetwork.CallRPC("SendTeamChat", UCNetwork.MessageReceiver.ServerOnly, -1, message);
    }

    public void OnPassTurn()
    {
        ClientNetwork.CallRPC("PassTurn", UCNetwork.MessageReceiver.ServerOnly, -1);
    }
    #endregion
}
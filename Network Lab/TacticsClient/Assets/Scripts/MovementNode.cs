﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementNode : MonoBehaviour
{
    private void Awake()
    {
        TacticsBoard.BoardCleared.AddListener(OnClearNodes);
    }
    private void OnClearNodes()
    {
        Destroy(gameObject);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable] public class Event_Int : UnityEvent<int> { }
[System.Serializable] public class Event_IntInt : UnityEvent<int, int>{}
[System.Serializable] public class Event_IntStr : UnityEvent<int, string>{}
[System.Serializable] public class Event_IntBool : UnityEvent<int, bool>{}
[System.Serializable] public class Event_IntIntInt : UnityEvent<int, int, int>{}
[System.Serializable] public class Event_Str : UnityEvent<string> {}


﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class AutoConnect : MonoBehaviour
{
    [Tooltip("Time in seconds from instantiation until server join.")]
    [SerializeField] private float _timeToJoin = 0.5f;
    [SerializeField] private string _ipFileName = "ip.txt";
    void Start()
    {
        StartCoroutine(JoinServer());
    }
    private IEnumerator JoinServer()
    {
        yield return new WaitForSeconds(_timeToJoin);
        TacticsClient client = GetComponent<TacticsClient>();
        if (client != null)
        {
            client.ConnectToServer();
            string[] ipText = System.IO.File.ReadAllLines("./" + _ipFileName);
            string ip = ipText[0];
            try
            {
                int port = int.Parse(ipText[1]);
            }
            catch
            {
                Application.Quit();
            }
        }
    }
}

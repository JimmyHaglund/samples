﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TacticsInput : MonoBehaviour
{
    [SerializeField] protected TacticsCreature _controlledCreature = null;
    public virtual TacticsCreature Creature { get { return _controlledCreature; } }
    public int Id = 0;
    public TacticsClient Client = null;

    #region Menu phase methods
    protected virtual void Ready(bool readyState)
    {
        if (Client != null)
        {
            Client.OnReady(true);
        }
    }
    public virtual void ChangeName(string newName)
    {
        if (Client != null)
        {
            Client.OnNameChange(newName);
        }
    }
    protected virtual void ChangeClass(int classType)
    {
        _controlledCreature.SetClass(classType);
        if (Client != null)
        {
            Client.OnClassChange(classType);
        }
    }
    protected virtual void ChangeClass(TacticsCreature.Class classType)
    {
        ChangeClass((int)classType);
    }
    #endregion

    #region Game phase methods
    public virtual void StartTurn() { }
    protected virtual void EndTurn(bool sendMessage = false)
    {
        if (sendMessage && Client != null)
        {
            Client.OnPassTurn();
        }
    }
    protected virtual void PassTurn()
    {
        if (Client != null)
        {
            Client.OnPassTurn();
        }
    }

    public virtual void SetUpNetwork(TacticsClient client)
    {
        Client = client;
    }

    protected virtual void Move(int targetX, int targetY)
    {
        if (Client != null)
        {
            Client.OnMove(targetX, targetY);
        }
    }
    protected virtual void Attack(int targetX, int targetY)
    {
        if (Client != null)
        {
            Client.OnAttack(targetX, targetY);
        }
    }
    
    protected virtual void PrintMessage()
    {
        if (Client != null)
        {
            Client.OnMessage();
        }
    }
    protected virtual void PrintTeamMessage(string message)
    {
        if (Client != null)
        {
            Client.OnTeamMessage(message);
        }
    }
    #endregion
}
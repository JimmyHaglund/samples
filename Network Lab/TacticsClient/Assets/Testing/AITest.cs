﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Class that slaps a bunch of AI character around randomly
public class AITest : MonoBehaviour
{
    [Header("Time per step on the board that a creature takes in seconds.")]
    [SerializeField] private float _stepTime = 0.25f;
    [SerializeField] private bool _showPathNodes = false;
    [SerializeField] private GameObject _goblinPrefab;
    private List<TacticsGoblinAI> _goblins;
    private int _currentGoblin;
    private void Start()
    {
        // Generate board
        TacticsBoard.SetBoardSize(10, 10);

        // Slap down some epic rocks
        for (int n = 0; n < 25; ++n)
        {
            TacticsBoard.BoardPosition position = RandomUnoccupiedPosition();
            TacticsBoard.SetSpaceBlocked(position.X, position.Y);
        }

        // Generate creatures
        _goblins = new List<TacticsGoblinAI>(40);
        for (int i = 0; i < 5; ++i)
        {
            _goblins.Add(GameObject.Instantiate(_goblinPrefab).GetComponent<TacticsGoblinAI>());
            TacticsBoard.BoardPosition position = RandomUnoccupiedPosition();
            _goblins[i].Creature.Teleport(position.X, position.Y);
            _goblins[i].gameObject.name = "Goblin " + i;
        }
        _currentGoblin = 0;
        _goblins[0].StartTurn();
        StartCoroutine(TimedTurns(1.0f));
        Camera.main.GetComponent<ConstantRotateAroundPoint>().SetRotation();
    }
    private void Update()
    {
        TacticsBoard.DebugEnabled = _showPathNodes;
    }

    private IEnumerator TimedTurns(float timer = 1.0f)
    {
        while (true)
        {
            yield return new WaitForSeconds(timer);
            if (!_goblins[_currentGoblin].Creature.Moving)
            {
                if (++_currentGoblin >= _goblins.Count)
                {
                    _currentGoblin = 0;
                    Random.InitState((int)Time.realtimeSinceStartup);
                }
                TacticsCreature currentCreature = _goblins[_currentGoblin].Creature;
                TacticsBoard.BoardPosition pos = RandomPositionInRangeOfCreatureThatIsAlsoUnoccupied(currentCreature);
                currentCreature.Move(pos.X, pos.Y, 0.05f);
                Debug.Log("Moving goblin: " + _currentGoblin);
                Debug.Log("Target space: " + pos.X + ", " + pos.Y);
                // _goblins[_currentGoblin].StartTurn();
            }
        }
    }

    private void RandomizeCreature(TacticsCreature creature)
    {
        creature.gameObject.name = ("Goblin " + _goblins.Count);
        creature.SetClass(Random.Range(1, 3));
        TacticsBoard.BoardPosition pos = RandomUnoccupiedPosition();
        creature.Teleport(pos.X, pos.Y);
    }
    private void MoveRandom(TacticsCreature creature)
    {
        TacticsBoard.BoardPosition position = RandomPosition();
        creature.Teleport(position.X, position.Y);
    }
    private TacticsBoard.BoardPosition RandomPositionInRangeOfCreature(TacticsCreature creature)
    {
        int moveRange = creature.GetClass().Moverange;
        int moveX = Random.Range(0, moveRange);
        int moveY = moveRange - moveX;
        if (Random.value > 0.5f) moveX *= -1;
        if (Random.value > 0.5f) moveY *= -1;

        TacticsBoard.BoardPosition move = new TacticsBoard.BoardPosition(moveX, moveY);
        move += creature.GetPosition();
        move.X = (int)Mathf.Clamp(move.X, 0, TacticsBoard.SizeX - 1);
        move.Y = (int)Mathf.Clamp(move.Y, 0, TacticsBoard.SizeY - 1);
        return (move);
    }
    // Best name ever? It's a test class.
    private TacticsBoard.BoardPosition RandomPositionInRangeOfCreatureThatIsAlsoUnoccupied(TacticsCreature creature)
    {
        TacticsBoard.BoardPosition position = RandomPositionInRangeOfCreature(creature);
        Stack<TacticsBoard.BoardPosition> path = _goblins[_currentGoblin].Creature.CheckPath(position.X, position.Y);
        int count = 0;
        while (count++ < 100 && TacticsBoard.GetSpace(position.X, position.Y) != null && path == null)
        {
            position = RandomPositionInRangeOfCreature(creature);
            path = _goblins[_currentGoblin].Creature.CheckPath(position.X, position.Y);
        }
        if (count > 99) Debug.Log("Tried to get a position too many times.");
        Debug.Log("Found target position at " + position.X + ", " + position.Y + ". At Position: " + TacticsBoard.GetSpace(position.X, position.Y));
        return position;
    }
    private TacticsBoard.BoardPosition RandomPosition()
    {
        int posX = Random.Range(0, TacticsBoard.SizeX - 1);
        int posY = Random.Range(0, TacticsBoard.SizeY - 1);

        return new TacticsBoard.BoardPosition(posX, posY);
    }

    private TacticsBoard.BoardPosition RandomUnoccupiedPosition()
    {
        TacticsBoard.BoardPosition position = RandomPosition();
        int count = 0;
        while (count < 100 && TacticsBoard.GetSpace(position.X, position.Y) != null)
        {
            position = RandomPosition();
        }
        return position;
    }
}

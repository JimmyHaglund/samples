﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class for testing path finding of TacticsBoard class.
/// </summary>
public class PathFindingTest : MonoBehaviour
{
    [SerializeField] private bool _ShowPathNodes = false;
    // Use this for initialization
    void Start()
    {
        TacticsBoard.DebugEnabled = true;
        TacticsBoard.SetBoardSize(20, 20);

        // Generate a wall to navigate around.
        for (int n = 1; n < 10; ++n)
        {
            TacticsBoard.SetSpaceBlocked(n, 5);
        }
        // Generate another wall just to make things difficult
        for (int i = 6; i < TacticsBoard.SizeY; ++i)
        {
            TacticsBoard.SetSpaceBlocked(1, i);
        }
        TacticsBoard.GetPath(0, 0, 5, 10);        
    }

    private void Update()
    {
        
        if (Input.GetButtonDown("Jump"))
        {
            PathFindRandom();
        }
        // Note: This is an incredibly stupid way to set this, but this is a tester script so I don't care.
        if (_ShowPathNodes)
        {
            TacticsBoard.DebugEnabled = true;
        }
        else
        {
            TacticsBoard.DebugEnabled = false;
        }
    }

    private void PathFindRandom()
    {
        int startX = Random.Range(0, TacticsBoard.SizeX);
        int startY = Random.Range(0, TacticsBoard.SizeY);
        int targetX = Random.Range(0, TacticsBoard.SizeX);
        int targetY = Random.Range(0, TacticsBoard.SizeY);

        Debug.Log("Generating new path from " + startX + ", " + startY + " to " + targetX + ", " + targetY);
        TacticsBoard.GetPath(startX, startY, targetX, targetY);
    }
}
